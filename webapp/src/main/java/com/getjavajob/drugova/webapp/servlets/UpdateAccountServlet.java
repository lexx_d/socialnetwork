//package com.getjavajob.drugova.webapp.servlets;
//
//import com.getjavajob.drugova.common.model.Account;
//import com.getjavajob.drugova.common.model.Phone;
//import com.getjavajob.drugova.dao.daoexceptions.DaoException;
//import com.getjavajob.drugova.sevice.AccountService;
//import org.springframework.context.ApplicationContext;
//import org.springframework.web.context.support.WebApplicationContextUtils;
//
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * Created by Александр on 09.07.2016.
// */
////@WebServlet(name = "UpdateServlet", urlPatterns = {"/updateServlet"})
//public class UpdateAccountServlet extends HttpServlet {
//    private AccountService accountService;
//
//    @Override
//    public void init() throws ServletException {
//        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(this.getServletContext());
//        accountService = applicationContext.getBean(AccountService.class);
//    }
//
//    @Override
//    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        /*
//        Account account = (Account) req.getSession().getAttribute("account");
//        String phonesArrayParam = req.getParameter("phonesArray");
//        String[] phonesArr = phonesArrayParam.split(",-,");
//        if (!phonesArr[0].equals("")) {
//            Map<String, Phone> phonesMap = new HashMap<>();
//            List<Phone> newPhoneList = new ArrayList<>();
//            for (String phoneRecord : phonesArr) {
//                String[] phoneRecordArray = phoneRecord.split(",");
//                Phone phone = new Phone(0, phoneRecordArray[0], phoneRecordArray[1]);
//                newPhoneList.add(phone);
//                phonesMap.put(phone.getNumber(), phone);
//            }
//
//            List<Phone> currentPhoneList = account.getPhones();
//
//            List<Phone> phonesForDelete = getPhonesForDelete(phonesMap, currentPhoneList);
//            List<Phone> phonesForInsert1 = getPhonesForInsert(newPhoneList, currentPhoneList);
//
//            account.setPhones(newPhoneList);//todo update from db
//        } else {
//            account.setPhones(null);//todo update from db
//        }
//        updateAccountInstance(req, account);
//
//        try {
//            accountService.updateAccount(account);
//        } catch (DaoException e) {
//            req.setAttribute("successUpdate", "true");
//            //resp.sendRedirect(req.getContextPath() + "/accountUpdate");
//            req.getRequestDispatcher(req.getContextPath() + "/accountUpdate").forward(req, resp);
//            return;
//        }
//
//        req.setAttribute("successUpdate", "false");
//        req.getRequestDispatcher(req.getContextPath() + "/accountUpdate").forward(req, resp);//resp.sendRedirect(req.getContextPath() + "/accountUpdate");
//        */
//    }
//
//    private List<Phone> getPhonesForInsert(List<Phone> newPhoneList, List<Phone> currentPhoneList) {
//        List<Phone> phonesForInsert = new ArrayList<>();
//        if (currentPhoneList != null) {
//            for (Phone phone : newPhoneList) {
//                int index = currentPhoneList.indexOf(phone);
//                if (index == -1 || !currentPhoneList.get(index).equals(phone)) {
//                    phonesForInsert.add(phone);
//                }
//            }
//        }
//        return phonesForInsert;
//    }
//
//    private List<Phone> getPhonesForDelete(Map<String, Phone> phonesMap, List<Phone> currentPhoneList) {
//        List<Phone> phonesForDelete = new ArrayList<>();
//        if (currentPhoneList != null) {
//            for (Phone phone : currentPhoneList) {
//                Phone phoneFromNewMap = phonesMap.get(phone.getNumber());
//                if (phoneFromNewMap == null || !phoneFromNewMap.equals(phone)) {
//                    phonesForDelete.add(phone);
//                }
//            }
//        }
//        return phonesForDelete;
//    }
//
//    private void updateAccountInstance(HttpServletRequest req, Account account) {
//        String name = req.getParameter("name");
//        String surname = req.getParameter("surname");
//        String patronymic = req.getParameter("patronymic");
//        String email = req.getParameter("email");
//        String icq = req.getParameter("icq");
//        String skype = req.getParameter("skype");
//        String address = req.getParameter("address");
//        String password = req.getParameter("password");
//        account.setAddress(address);
//        account.setPatronymic(patronymic);
//        account.setIcq(icq);
//        account.setSkype(skype);
//        account.setPassword(password);
//        account.setName(name);
//        account.setSurname(surname);
//    }
//}
