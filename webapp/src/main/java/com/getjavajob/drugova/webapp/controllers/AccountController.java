package com.getjavajob.drugova.webapp.controllers;

import com.getjavajob.drugova.common.model.Account;
import com.getjavajob.drugova.common.model.FriendRequest;
import com.getjavajob.drugova.common.model.Message;
import com.getjavajob.drugova.common.model.Phone;
import com.getjavajob.drugova.dao.daoexceptions.DaoException;
import com.getjavajob.drugova.sevice.AccountService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
public class AccountController {
    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

    @Autowired
    private AccountService accountService;

    @Value("${accountController.wallMessagesPerPage}")
    private int wallMessagesPerPage;

    @RequestMapping(value = {"/", "/main"})
    public ModelAndView defaultController() {
        return new ModelAndView("index");
    }

    @RequestMapping(value = "/AllAccountsServlet", method = RequestMethod.GET)
    public ModelAndView showAllAccounts() {
        logger.debug("method showAllAccounts called");
        List<Account> accounts = null;
        try {
            accounts = accountService.getAllAccounts();
        } catch (DaoException e) {
            e.printStackTrace();
        }

        ModelAndView modelAndView = new ModelAndView("allAccounts");
        modelAndView.addObject("accounts", accounts);
        return modelAndView;
    }

    @RequestMapping(value = "/loginChecker", method = RequestMethod.POST)
    public String checkLogin(@RequestParam("email") String email, @RequestParam("password") String password,
                             @RequestParam("rememberMe") String rm, HttpServletRequest req, HttpServletResponse resp) {
        Account account;
        try {
            account = accountService.getAccountByMailAndPassword(email, password);
        } catch (DaoException ex) {
            account = null;
        }

        if (account != null) {
            req.getSession().setAttribute("account", account);
            logger.info("session created for account with id = {}", account.getId());
            if (rm != null) {
                Cookie cookie = new Cookie("socialNetAccountId", String.valueOf(account.getId()));
                cookie.setMaxAge(60 * 60 * 24);
                resp.addCookie(cookie);
                logger.info("cookie added for user with account id = {}", account.getId());

            }
            logger.info("user with account id = {} authorized", account.getId());
            return "redirect:/personalPage?id=" + account.getId();
        } else {
            logger.info("authorization failed");
            return "redirect:/main?login=error";
        }
    }

    @RequestMapping(value = "/personalPage", method = RequestMethod.GET)
    public ModelAndView accountPage(@RequestParam(value = "id", required = false) Integer accountId,
                                    ModelAndView modelAndView, HttpServletRequest req,
                                    RedirectAttributes redir) throws DaoException {
        Account sessionAccount = (Account) req.getSession().getAttribute("account");
        sessionAccount = accountService.getAccountById(sessionAccount.getId());

        //calc page count for wall messages
        int id = accountId == null ? sessionAccount.getId() : accountId;
        long messagesCount = accountService.getWallMessagesCount(id);
        long wallPageCount = Math.round(messagesCount / (wallMessagesPerPage + .0) + 0.5);

        //redirect to session account personal page
        if (accountId == null) {
            modelAndView.setViewName("redirect:/personalPage?id=" + sessionAccount.getId());
            redir.addFlashAttribute("wallPageCount", wallPageCount);
            logger.info("account id = {} went to its own personal page", sessionAccount.getId());
            return modelAndView;
        }

        try {
            Account account = accountService.getAccountById(accountId);
            modelAndView.addObject("account", account);
            modelAndView.addObject("wallPageCount", wallPageCount);
            if (isAccountInFriends(sessionAccount, account)) {
                modelAndView.addObject("friendStatus", "accepted");
            } else if (isAccountInOutRequests(sessionAccount, account)) {
                modelAndView.addObject("friendStatus", "outgoing");
            } else if (isAccountInInRequests(sessionAccount, account)) {
                modelAndView.addObject("friendStatus", "incoming");
            } else {
                modelAndView.addObject("friendStatus", "none");
            }
        } catch (DaoException e) {
            e.printStackTrace();
        }
        logger.info("account id = {} went to the personal page of account id = {}", sessionAccount.getId(), accountId);
        return modelAndView;
    }

    private boolean isAccountInFriends(Account sessionAccount, Account account) {
        List<Account> friends = getAcceptedFriends(sessionAccount);
        for (Account friend : friends) {
            if (friend.getId() == account.getId()) {
                return true;
            }
        }
        return false;
    }

    private boolean isAccountInOutRequests(Account sessionAccount, Account account) {
        List<Account> friends = getOutgoingFriends(sessionAccount);
        for (Account friend : friends) {
            if (friend.getId() == account.getId()) {
                return true;
            }
        }
        return false;
    }

    private boolean isAccountInInRequests(Account sessionAccount, Account account) {
        List<Account> friends = getIncomingFriends(sessionAccount);
        for (Account friend : friends) {
            if (friend.getId() == account.getId()) {
                return true;
            }
        }
        return false;
    }

    @RequestMapping(value = "/{id:[0-9]+}/friends")
    public ModelAndView showFriends(@PathVariable("id") int accountId) {
        logger.info("account id = {} tried to show its friends", accountId);
        ModelAndView modelAndView = new ModelAndView("/friends");
        try {
            Account account = accountService.getAccountById(accountId);
            List<Account> friends = getAcceptedFriends(account);// = accountService.getFriends(accountId);
            modelAndView.addObject("friends", friends);
        } catch (DaoException e) {
            e.printStackTrace();
        }

        return modelAndView;
    }

    private List<Account> getAcceptedFriends(Account account) {
        List<Account> friends = new ArrayList<>();
        for (FriendRequest friendRequest : account.getOutFriendRequests()) {
            if (friendRequest.isAccepted()) {
                friends.add(friendRequest.getAccountTo());
            }
        }
        for (FriendRequest friendRequest : account.getInFriendRequests()) {
            if (friendRequest.isAccepted()) {
                friends.add(friendRequest.getAccountFrom());
            }
        }
        return friends;
    }

    @RequestMapping(value = "/logout")
    public String logout(HttpServletRequest req, HttpServletResponse resp) {
        HttpSession session = req.getSession();
        Account sessionAccount = (Account)session.getAttribute("account");
        logger.info("account id = {} logged out", sessionAccount.getId());

        session.invalidate();
        Cookie[] cookies = req.getCookies();
        for (Cookie cookie : cookies) {
            cookie.setMaxAge(0);
            cookie.setValue(null);
            resp.addCookie(cookie);
            logger.info("account id = {} cookies cleared", sessionAccount.getId());
        }

        return "redirect:/main";
    }

    @RequestMapping(value = "/showUpdateInfo")
    public ModelAndView showUpdateInfo() {
        return new ModelAndView("update");
    }

    @RequestMapping(value = "/updateServlet", method = RequestMethod.POST)
    public ModelAndView updateAccount(@RequestParam("file") MultipartFile file,
                                      @ModelAttribute Account account, @RequestParam("phonesArray") String phonesArrayParam,
                                      RedirectAttributes redirectAttributes, HttpServletRequest req) throws ServletException, IOException {
        String[] phonesArr = phonesArrayParam.split(",-,");
        if (!phonesArr[0].equals("")) {
            Map<String, Phone> phonesMap = new HashMap<>();
            List<Phone> newPhoneList = new ArrayList<>();
            for (String phoneRecord : phonesArr) {
                String[] phoneRecordArray = phoneRecord.split(",");
                Phone phone = new Phone(phoneRecordArray[0], phoneRecordArray[1]);
                newPhoneList.add(phone);
                phonesMap.put(phone.getNumber(), phone);
            }

            List<Phone> currentPhoneList = account.getPhones();

            List<Phone> phonesForDelete = getPhonesForDelete(phonesMap, currentPhoneList);
            List<Phone> phonesForInsert1 = getPhonesForInsert(newPhoneList, currentPhoneList);

            account.setPhones(newPhoneList);//todo update from db
        } else {
            account.setPhones(null);//todo update from db
        }

        Account sessionAccount = (Account) req.getSession().getAttribute("account");
        if (!file.isEmpty()) {
            byte[] bytes = file.getBytes();
            account.setPhoto(bytes);
            //refresh session account photo
            sessionAccount.setPhoto(bytes);
        } else {
            //leave photo unchanged
            account.setPhoto(sessionAccount.getPhoto());
        }

        ModelAndView modelAndView = new ModelAndView("redirect:/showUpdateInfo");
        try {
            accountService.updateAccount(account);
        } catch (DaoException e) {
            modelAndView.addObject("successUpdate", "false");
            return modelAndView;
        }

        modelAndView.addObject("successUpdate", "true");
        return modelAndView;
    }

    private List<Phone> getPhonesForInsert(List<Phone> newPhoneList, List<Phone> currentPhoneList) {
        List<Phone> phonesForInsert = new ArrayList<>();
        if (currentPhoneList != null) {
            for (Phone phone : newPhoneList) {
                int index = currentPhoneList.indexOf(phone);
                if (index == -1 || !currentPhoneList.get(index).equals(phone)) {
                    phonesForInsert.add(phone);
                }
            }
        }
        return phonesForInsert;
    }

    private List<Phone> getPhonesForDelete(Map<String, Phone> phonesMap, List<Phone> currentPhoneList) {
        List<Phone> phonesForDelete = new ArrayList<>();
        if (currentPhoneList != null) {
            for (Phone phone : currentPhoneList) {
                Phone phoneFromNewMap = phonesMap.get(phone.getNumber());
                if (phoneFromNewMap == null || !phoneFromNewMap.equals(phone)) {
                    phonesForDelete.add(phone);
                }
            }
        }
        return phonesForDelete;
    }

    @RequestMapping("/accountRegistration")
    public ModelAndView showRegistrationPage() {
        return new ModelAndView("registration");
    }

    public String registerAccount(@ModelAttribute Account account, @RequestParam("phonesArray") String phonesArrayParam,
                                  RedirectAttributes redirectAttributes) throws ServletException, IOException {
        try {

            int newAccountId = accountService.createAccount(account);
            logger.info("new account with id = {} created", newAccountId);
        } catch (DaoException e) {
            e.printStackTrace();
            logger.warn("failed account creating with email = {}", account.getEmail());
        }

        return "redirect:/";
    }

    @RequestMapping(value = "/showAccounts", method = RequestMethod.GET)
    @ResponseBody
    public List<Account> showAccounts(@RequestParam("filter") final String filter) {
        try {
            List<Account> allAccouts = accountService.getAllAccounts();
            CollectionUtils.filter(allAccouts, new Predicate<Account>() {
                @Override
                public boolean evaluate(Account account) {
                    return account.getName().contains(filter) || account.getSurname().contains(filter);
                }
            });
            for (Account account : allAccouts) {
                account.setPassword(null);
                account.setGroups(null);
                account.setFriends(null);
                account.setPhones(null);
                account.setOutFriendRequests(null);
                account.setInFriendRequests(null);
                account.setInMessages(null);
                account.setOutMessages(null);
                account.setPhoto(null);
            }
            return allAccouts;
        } catch (DaoException e) {
            e.printStackTrace();
        }

        return null;
    }

    @RequestMapping("/avatar")
    @ResponseBody
    public byte[] getAvatar(@RequestParam(value = "id", required = false) Integer accountId, HttpServletRequest req) {
        if (accountId == null) {
            Account sessionAccount = (Account) req.getSession().getAttribute("account");
            if (sessionAccount != null) {
                return sessionAccount.getPhoto();
            }
        } else {
            try {
                Account account = accountService.getAccountById(accountId);
                if (account != null) {
                    return account.getPhoto();
                }
            } catch (DaoException e) {
                return null;
            }
        }
        return null;
    }

    @RequestMapping("/showUpdateMainInfo")
    public ModelAndView updateMainInfoPage(HttpServletRequest req) {
        return new ModelAndView("updateMainInfo");
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(
                dateFormat, false));
    }

    @RequestMapping("/updateMainInfo")
    public ModelAndView updateMainInfo(@RequestParam("file") MultipartFile file,
                                       @ModelAttribute Account account, HttpServletRequest req) throws IOException {
        Account sessionAccount = (Account) req.getSession().getAttribute("account");
        if (!file.isEmpty()) {
            byte[] bytes = file.getBytes();
            account.setPhoto(bytes);
            //refresh session account photo
            sessionAccount.setPhoto(bytes);
        } else {
            //leave photo unchanged
            account.setPhoto(sessionAccount.getPhoto());
        }

        sessionAccount.setAddress(account.getAddress());
        sessionAccount.setBirthDate(account.getBirthDate());
        sessionAccount.setEmail(account.getEmail());
        sessionAccount.setIcq(account.getIcq());
        sessionAccount.setName(account.getName());
        sessionAccount.setSurname(account.getSurname());
        sessionAccount.setPatronymic(account.getPatronymic());
        sessionAccount.setSkype(account.getSkype());

        ModelAndView modelAndView = new ModelAndView("redirect:/showUpdateInfo#mainInfo");
        try {
            accountService.updateAccount(sessionAccount);
        } catch (DaoException e) {
            modelAndView.addObject("successUpdate", "false");
            return modelAndView;
        }

        modelAndView.addObject("successUpdate", "true");
        return modelAndView;
    }

    @RequestMapping("/showUpdatePhones")
    public ModelAndView updatePhonesPage() {
        return new ModelAndView("updatePhones");
    }

    @RequestMapping("/updatePhones")
    public ModelAndView updatePhones(@RequestParam("phonesArray") String phonesArrayParam,
                                     RedirectAttributes redirectAttributes, HttpServletRequest req) {

        Account sessionAccount = (Account) req.getSession().getAttribute("account");
        try {
            sessionAccount = accountService.getAccountById(sessionAccount.getId());
        } catch (DaoException e) {
            sessionAccount = null;
        }
        String[] phonesArr = phonesArrayParam.split(",,");
        if (!phonesArr[0].equals("")) {
            Map<String, Phone> phonesMap = new HashMap<>();
            List<Phone> newPhoneList = new ArrayList<>();
            for (String phoneRecord : phonesArr) {
                String[] phoneRecordArray = phoneRecord.split(",");
                Phone phone = new Phone(phoneRecordArray[0], phoneRecordArray[1]);
                newPhoneList.add(phone);
                phonesMap.put(phone.getNumber(), phone);
            }

            //List<Phone> currentPhoneList = sessionAccount.getPhones();

            //List<Phone> phonesForDelete = getPhonesForDelete(phonesMap, currentPhoneList);
            //List<Phone> phonesForInsert1 = getPhonesForInsert(newPhoneList, currentPhoneList);

            //sessionAccount.setPhones(newPhoneList);
            sessionAccount.getPhones().clear();
            sessionAccount.getPhones().addAll(0, newPhoneList);
        } else {
            sessionAccount.getPhones().clear();
        }
        req.getSession().setAttribute("account", sessionAccount);//todo remove ???
        ModelAndView modelAndView = new ModelAndView("redirect:/showUpdateInfo#mainInfo");
        try {
            accountService.updateAccount(sessionAccount);
        } catch (DaoException e) {
            modelAndView.addObject("successUpdate", "false");
            return modelAndView;
        }

        modelAndView.addObject("successUpdate", "true");
        return modelAndView;
    }

    @RequestMapping("/showUpdatePassword")
    public ModelAndView updatePasswordPage() {
        return new ModelAndView("updatePassword");
    }

    @RequestMapping("/showAcceptedFriends")
    public ModelAndView showAcceptedFriends(HttpServletRequest req) throws DaoException {
        ModelAndView modelAndView = new ModelAndView("acceptedFriends");
        Account sessionAccount = (Account) req.getSession().getAttribute("account");
        Account account = accountService.getAccountById(sessionAccount.getId());
        List<Account> friends = getAcceptedFriends(account);
        modelAndView.addObject("friends", friends);
        return modelAndView;
    }

    @RequestMapping("/showIncomingFriends")
    public ModelAndView showIncomingFriends(HttpServletRequest req) throws DaoException {
        ModelAndView modelAndView = new ModelAndView("inFriendRequests");
        Account sessionAccount = (Account) req.getSession().getAttribute("account");
        Account account = accountService.getAccountById(sessionAccount.getId());
        List<Account> friends = getIncomingFriends(account);
        modelAndView.addObject("incomingFriends", friends);
        return modelAndView;
    }

    @RequestMapping("/showOutgoingFriends")
    public ModelAndView showOutgoingFriends(HttpServletRequest req) throws DaoException {
        ModelAndView modelAndView = new ModelAndView("outFriendRequests");
        Account sessionAccount = (Account) req.getSession().getAttribute("account");
        Account account = accountService.getAccountById(sessionAccount.getId());
        List<Account> friends = getOutgoingFriends(account);
        modelAndView.addObject("outgoingFriends", friends);
        return modelAndView;
    }

    private List<Account> getIncomingFriends(Account account) {
        List<Account> resultList = new ArrayList<>();
        for (FriendRequest friendRequest : account.getInFriendRequests()) {
            if (!friendRequest.isAccepted()) {
                resultList.add(friendRequest.getAccountFrom());
            }
        }
        return resultList;
    }

    private List<Account> getOutgoingFriends(Account account) {
        List<Account> resultList = new ArrayList<>();
        for (FriendRequest friendRequest : account.getOutFriendRequests()) {
            if (!friendRequest.isAccepted()) {
                resultList.add(friendRequest.getAccountTo());
            }
        }
        return resultList;
    }

    @RequestMapping("/addFriend")
    public ModelAndView addFriend(@RequestParam("id") int id, HttpServletRequest req) throws DaoException {
        Account sAccount = (Account) req.getSession().getAttribute("account");
        accountService.addFriendRequest(sAccount.getId(), id);
        logger.info("account id = {} sent friend request to account id = {}", sAccount.getId(), id);
        return new ModelAndView("redirect:/personalPage?id=" + id);
    }

    @RequestMapping("/deleteFriendRequest")
    public ModelAndView deleteFriendRequest(@RequestParam("id") int id, HttpServletRequest req) throws DaoException {
        Account sAccount = (Account) req.getSession().getAttribute("account");
        FriendRequest friendRequest = getRequest(sAccount, id);
        accountService.deleteFriendRequest(friendRequest.getId());
        logger.info("account id = {} delete friend request to account id = {}", sAccount.getId(), id);
        return new ModelAndView("redirect:/personalPage?id=" + id);
    }

    @RequestMapping("/acceptFriendRequest")
    public ModelAndView acceptFriendRequest(@RequestParam("id") int id, HttpServletRequest req) throws DaoException {
        Account sAccount = (Account) req.getSession().getAttribute("account");
        FriendRequest friendRequest = getRequest(sAccount, id);
        accountService.acceptFriendRequest(friendRequest);
        logger.info("account id = {} accept friend request from account id = {}", sAccount.getId(), id);
        return new ModelAndView("redirect:/personalPage?id=" + id);
    }

    private FriendRequest getRequest(Account sessionAccount, int friendId) {
        for (FriendRequest friendRequest : sessionAccount.getOutFriendRequests()) {
            if (friendRequest.getAccountTo().getId() == friendId) {
                return friendRequest;
            }
        }

        for (FriendRequest friendRequest : sessionAccount.getInFriendRequests()) {
            if (friendRequest.getAccountFrom().getId() == friendId) {
                return friendRequest;
            }
        }
        return null;
    }

    @RequestMapping("/wall")
    @ResponseBody
    public ModelAndView showWallPage(@RequestParam("accountId") int id, @RequestParam("pageNumber") int pageNumber) throws DaoException {
        ModelAndView modelAndView = new ModelAndView("wall");
        int beginPosFormMsg = pageNumber - 1;
        List<Message> messages = accountService.getWallMessages(id, beginPosFormMsg * wallMessagesPerPage, beginPosFormMsg + wallMessagesPerPage);
        modelAndView.addObject("messages", messages);
        return modelAndView;
    }

    @RequestMapping("/addWallMessage")
    public ModelAndView addWallMessage(@RequestParam("accountId") int id, @RequestParam("newWallMessage") String text,
                                       HttpServletRequest req, ModelAndView modelAndView, RedirectAttributes redir) {
        Message message = new Message();
        try {
            int sessionAccountId = ((Account) req.getSession().getAttribute("account")).getId();
            accountService.addAccountMessage(sessionAccountId, id, Message.MessageType.WALL, text, null);
            logger.info("account id = {} add message to wall of account id = {}", sessionAccountId, id);
        } catch (DaoException e) {
            return modelAndView;
        }
        modelAndView.setViewName("redirect:/personalPage?id=" + id);
        redir.addFlashAttribute("wallPageCount", modelAndView.getModel().get("wallPageCount"));
        return modelAndView;
    }

    @RequestMapping(value = "/deleteWallMessage/{idMessage:[0-9]+}/{accountId:[0-9]+}")
    public ModelAndView deleteWallMessage(@PathVariable("accountId") int id,
                                          @PathVariable("idMessage") int messageId) throws DaoException {
        accountService.deleteMessage(messageId);
        logger.info("account id = {} delete message with id = {}", id, messageId);
        return new ModelAndView("redirect:/personalPage?id=" + id);
    }

    @RequestMapping(value = "/chat")
    public ModelAndView showChat() {
        return new ModelAndView("chat");
    }

    @RequestMapping(value = "/showChat")
    public ModelAndView showDialog(@RequestParam("receiverId") int receiverId, @RequestParam("authorId") int authorId) throws DaoException {
        ModelAndView modelAndView = new ModelAndView("chat");
        List<Message> messages = accountService.getMessagesBetweenAccounts(authorId, receiverId);
        modelAndView.addObject("messages", messages);
        modelAndView.addObject("receiverId", receiverId);
        return modelAndView;
    }

    @RequestMapping(value = "/sendPrivateMessage")
    public ModelAndView sendPrivateMessage(@RequestParam("authorId") int authorId,
                                           @RequestParam("receiverId") int receiverId,
                                           @RequestParam("text") String text) throws DaoException {

        accountService.addAccountMessage(authorId, receiverId, Message.MessageType.PRIVATE, text, null);
        ModelAndView modelAndView = new ModelAndView("chat");
        List<Message> messages = accountService.getMessagesBetweenAccounts(authorId, receiverId);
        modelAndView.addObject("messages", messages);
        modelAndView.addObject("receiverId", receiverId);
        return modelAndView;
    }
}
