//package com.getjavajob.drugova.webapp.filters;
//
//import com.getjavajob.drugova.common.model.Account;
//import com.getjavajob.drugova.dao.daoexceptions.DaoException;
//import com.getjavajob.drugova.sevice.AccountService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.context.support.SpringBeanAutowiringSupport;
//
//import javax.servlet.*;
//import javax.servlet.http.Cookie;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//import java.io.IOException;
//
///**
// * Created by Александр on 23.06.2016.
// */
//public class LoginFilter implements Filter {
//    @Autowired
//    private AccountService accountService;
//
//    @Override
//    public void init(FilterConfig filterConfig) throws ServletException {
//        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this,
//                filterConfig.getServletContext());
//    }
//
//    @Override
//    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
//        HttpServletRequest request = (HttpServletRequest) servletRequest;
//        HttpServletResponse response = (HttpServletResponse) servletResponse;
//        String servletPath = request.getServletPath();
//        if (servletPath.contains("/resources")) {
//            filterChain.doFilter(request, response);
//            return;
//        }
//
//        boolean needAuthorization = true;
//
//        HttpSession session = request.getSession(false);
//        if (session != null && session.getAttribute("account") != null) {
//            needAuthorization = false;
//        } else {
//            Cookie[] cookies = request.getCookies();
//            if (cookies != null) {
//                try {
//                    for (Cookie cookie : cookies) {
//                        if (cookie.getName().equals("socialNetAccountId")) {
//                            int accountId = Integer.valueOf(cookie.getValue());
//                            Account account = accountService.getAccountById(accountId);
//
//                            if (account != null) {
//                                needAuthorization = false;
//                                session = request.getSession();
//                                session.setAttribute("account", account);
//                            }
//                            break;
//                        }
//                    }
//                } catch (DaoException ex) {
//                    ex.printStackTrace();
//                    //empty block => redirect to authorization page
//                }
//            }
//        }
//
//        if (needAuthorization) {
//            if (servletPath.contains("/index.jsp") || servletPath.contains("main") ||
//                    servletPath.contains("egistration") || servletPath.contains("loginChecker")) { //allow this urls without authorization
//                filterChain.doFilter(servletRequest, servletResponse);
//                return;
//            }
//            response.sendRedirect(request.getContextPath() + "/main");
//        } else {
//            if (servletPath.contains("index.jsp") || servletPath.contains("/main")) {
//                response.sendRedirect("/personalPage"); //user already authorized => redirect to main page
//                return;
//            }
//
//            filterChain.doFilter(servletRequest, servletResponse);
//        }
//    }
//
//    @Override
//    public void destroy() {
//
//    }
//}
