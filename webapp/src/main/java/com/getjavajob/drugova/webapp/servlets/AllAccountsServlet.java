package com.getjavajob.drugova.webapp.servlets;

import com.getjavajob.drugova.common.model.Account;
import com.getjavajob.drugova.dao.daoexceptions.DaoException;
import com.getjavajob.drugova.sevice.AccountService;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class AllAccountsServlet extends HttpServlet {
    private AccountService accountService;

    @Override
    public void init() throws ServletException {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(this.getServletContext());
        accountService = applicationContext.getBean(AccountService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //AccountService accountService = new AccountService();

        List<Account> accounts = null;
        try {
            accounts = accountService.getAllAccounts();
        } catch (DaoException e) {
            e.printStackTrace();
        }

        req.setAttribute("accounts", accounts);
        req.getRequestDispatcher("/WEB-INF/jsp/allAccounts.jsp").forward(req, resp);
    }
    /*
    private void outputAllAccounts(HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html; charset=UTF8");
        PrintWriter writer = resp.getWriter();
        AccountService accountService = new AccountService();
        List<Account> accounts = accountService.getAllAccounts();
        writer.write("<table align=\"center\" border=\"2\" width=\"85%\">");
        writer.write("<tr><td>" + "№" + "</td><td>" + "Фамилия" + "</td><td>" + "Дата рождения" + "</td><td>" + "Скайп" + "</td></tr>");
        for (Account account : accounts) {
            writer.write("<tr><td>" + account.getId() + "</td>");
            writer.write("</td><td>" + account.getSurname() + "</td>");
            writer.write("</td><td>" + account.getBirthDate() + "</td>");
            writer.write("</td><td>" + account.getSkype() + "</td><tr>");
        }
        writer.write("</table>");
    }
    */
}
