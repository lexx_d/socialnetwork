//package com.getjavajob.drugova.webapp.servlets;
//
//import com.getjavajob.drugova.common.model.Account;
//import com.getjavajob.drugova.dao.daoexceptions.DaoException;
//import com.getjavajob.drugova.sevice.AccountService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.context.support.SpringBeanAutowiringSupport;
//
//import javax.servlet.ServletConfig;
//import javax.servlet.ServletException;
//import javax.servlet.http.*;
//import java.io.IOException;
//
///**
// * Created by Александр on 23.06.2016.
// */
//public class LoginChecker extends HttpServlet {
//    @Autowired
//    private AccountService accountService;
//
//    public void init(ServletConfig config) throws ServletException {
//        super.init(config);
//        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this,
//                config.getServletContext());
//    }
//
//    @Override
//    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        String email = req.getParameter("email");
//        String pswdHash = req.getParameter("password");
//        Account account = null;
//        try {
//            account = accountService.getAccountByMailAndPassword(email, pswdHash);
//        } catch (DaoException ex) {
//
//        }
//
//        if (account != null) {
//            HttpSession session = req.getSession();
//            session.setAttribute("account", account);
//            if (req.getParameter("rememberMe") != null) {
//                Cookie cookie = new Cookie("socialNetAccountId", String.valueOf(account.getId()));
//                cookie.setMaxAge(60 * 60 * 24);
//                resp.addCookie(cookie);
//            }
//            resp.sendRedirect(req.getContextPath() + "/personalPage");
//        } else {
//            resp.sendRedirect(req.getContextPath() + "/main?login=error");
//        }
//    }
//}
