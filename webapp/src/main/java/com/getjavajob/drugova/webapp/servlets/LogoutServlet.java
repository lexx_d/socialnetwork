//package com.getjavajob.drugova.webapp.servlets;
//
//import javax.servlet.ServletException;
//import javax.servlet.http.Cookie;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//
///**
// * Created by Александр on 24.06.2016.
// */
////@WebServlet(name = "LogoutServlet", urlPatterns = {"/logout"})
//public class LogoutServlet extends HttpServlet {
//    private void processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        req.getSession().invalidate();
//        Cookie[] cookies = req.getCookies();
//        for (Cookie cookie : cookies) {
//            cookie.setMaxAge(0);
//            cookie.setValue(null);
//            resp.addCookie(cookie);
//        }
//
//        resp.sendRedirect(req.getContextPath() + "/main");
//    }
//
//    @Override
//    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        processRequest(req, resp);
//    }
//
//    @Override
//    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        processRequest(req, resp);
//    }
//}
