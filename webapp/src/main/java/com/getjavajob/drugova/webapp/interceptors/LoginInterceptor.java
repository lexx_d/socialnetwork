package com.getjavajob.drugova.webapp.interceptors;

import com.getjavajob.drugova.common.model.Account;
import com.getjavajob.drugova.dao.daoexceptions.DaoException;
import com.getjavajob.drugova.sevice.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginInterceptor extends HandlerInterceptorAdapter {
    @Autowired
    AccountService accountService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String servletPath = request.getServletPath();
        if (servletPath.contains("/resources")) {
            return true;
        }

        boolean needAuthorization = true;

        HttpSession session = request.getSession(false);
        if (session != null && session.getAttribute("account") != null) {
            needAuthorization = false;
        } else {
            Cookie[] cookies = request.getCookies();
            if (cookies != null) {
                try {
                    for (Cookie cookie : cookies) {
                        if (cookie.getName().equals("socialNetAccountId")) {
                            int accountId = Integer.valueOf(cookie.getValue());
                            Account account = accountService.getAccountById(accountId);

                            if (account != null) {
                                needAuthorization = false;
                                session = request.getSession();
                                session.setAttribute("account", account);
                            }
                            break;
                        }
                    }
                } catch (DaoException ex) {
                    ex.printStackTrace();
                    //empty block => redirect to authorization page
                }
            }
        }

        if (needAuthorization) {
            if (servletPath.contains("/index.jsp") || servletPath.contains("main") ||
                    servletPath.contains("egistration") || servletPath.contains("loginChecker")) { //allow this urls without authorization
                return true;
            }
            response.sendRedirect(request.getContextPath() + "/main");
            return false;
        } else {
            if (servletPath.contains("index.jsp") || servletPath.contains("/main") || servletPath.equals("/")) {
                response.sendRedirect("/personalPage"); //user already authorized => redirect to main page
                return false;
            }

            return true;
        }
    }
}
