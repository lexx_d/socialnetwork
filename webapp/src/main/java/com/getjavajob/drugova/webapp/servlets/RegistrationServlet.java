//package com.getjavajob.drugova.webapp.servlets;
//
//import com.getjavajob.drugova.common.model.Account;
//import com.getjavajob.drugova.dao.daoexceptions.DaoException;
//import com.getjavajob.drugova.sevice.AccountService;
//import org.springframework.context.ApplicationContext;
//import org.springframework.web.context.support.WebApplicationContextUtils;
//
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//
///**
// * Created by Александр on 22.06.2016.
// */
//public class RegistrationServlet extends HttpServlet {
//    private AccountService accountService;
//
//    @Override
//    public void init() throws ServletException {
//        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(this.getServletContext());
//        accountService = applicationContext.getBean(AccountService.class);
//    }
//
//    @Override
//    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        String name = req.getParameter("name");
//        String surname = req.getParameter("surname");
//        String patronymic = req.getParameter("patronymic");
//        String email = req.getParameter("email");
//        String icq = req.getParameter("icq");
//        String skype = req.getParameter("skype");
//        String address = req.getParameter("address");
//        String password = req.getParameter("password");
//        Account account = new Account(name, surname, email);
//        account.setAddress(address);
//        account.setPatronymic(patronymic);
//        account.setIcq(icq);
//        account.setSkype(skype);
//        account.setPassword(password);
//
//        try {
//            accountService.createAccount(account);
//        } catch (DaoException e) {
//            e.printStackTrace();
//        }
//
//        resp.sendRedirect(req.getContextPath() + "/AllAccountsServlet");
//    }
//}
