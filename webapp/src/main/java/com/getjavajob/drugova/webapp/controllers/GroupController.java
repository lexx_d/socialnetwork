package com.getjavajob.drugova.webapp.controllers;

import com.getjavajob.drugova.common.model.Account;
import com.getjavajob.drugova.common.model.Group;
import com.getjavajob.drugova.common.model.GroupRequest;
import com.getjavajob.drugova.dao.daoexceptions.DaoException;
import com.getjavajob.drugova.sevice.GroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

@Controller
public class GroupController {
    private static final Logger logger = LoggerFactory.getLogger(GroupController.class);

    @Autowired
    private GroupService groupService;

    @Value("${accountController.wallMessagesPerPage}")
    private int wallMessagesPerPage;

    @Value("${groupController.membersAmountOnGroupPage}")
    private int membersAmountOnGroupPage;

    @Value("${groupController.adminAmountOnGroupPage}")
    private int adminAmountOnGroupPage;

    @RequestMapping("/groupAvatar")
    @ResponseBody
    public byte[] getAvatar(@RequestParam(value = "id", required = false) Integer groupId, HttpServletRequest req) {
        if (groupId == null) {
            Account sessionAccount = (Account) req.getSession().getAttribute("account");
            if (sessionAccount != null) {
                return sessionAccount.getPhoto();
            }
        } else {
            try {
                Group group = groupService.getGroupById(groupId);
                if (group != null) {
                    return group.getImage();
                }
            } catch (DaoException e) {
                return null;
            }
        }

        return null;
    }

    @RequestMapping("/showAllGroups")
    public ModelAndView showAllGroups() throws DaoException {
        ModelAndView modelAndView = new ModelAndView("/allGroups");
        List<Group> groups = groupService.getAllGroups();
        modelAndView.addObject("groups", groups);
        return modelAndView;
    }

    @RequestMapping("/showGroupPage")
    public ModelAndView showGroupPage() throws DaoException {
        return new ModelAndView("groupPage");
    }

    @RequestMapping("/showGroupCreatingForm")
    public ModelAndView showGroupCreatingForm() {
        return new ModelAndView("groupCreatingForm");
    }

    @RequestMapping("/createGroup")
    public ModelAndView createGroup(@RequestParam("id") int creatorId,
                                    @RequestParam("file") MultipartFile file,
                                    @RequestParam("description") String description,
                                    @RequestParam("groupName") String groupName,
                                    HttpServletRequest req,
                                    ModelAndView modelAndView,
                                    RedirectAttributes redir) throws DaoException {

        Account sessionAccount = (Account) req.getSession().getAttribute("account");
        Group newGroup = new Group();
        newGroup.setCreator(sessionAccount);
        newGroup.setDescription(description);

        byte[] bytes = null;
        try {
            if (!file.isEmpty()) {
                bytes = file.getBytes();
                newGroup.setImage(bytes);
            }
        } catch (IOException e) {
            logger.warn("IOException in method createGroup. Couldn't save image");
        }

        int createdGroupId = groupService.createGroup(sessionAccount.getId(), groupName, description, bytes);

        modelAndView.setViewName("redirect:/groupPage?id=" + createdGroupId);

        redir.addFlashAttribute("userType", "admin");

        return modelAndView;
    }

    @RequestMapping("/groupPage")
    public ModelAndView showGroupPage(@RequestParam(value = "id", required = false) Integer groupId,
                                      @SessionAttribute("account") Account sessionAccount) {
        GroupRequest groupRequest = getGroupRequest(sessionAccount, groupId);
        try {
            Group group = groupService.getGroupById(groupId);
            ModelAndView modelAndView = new ModelAndView("groupPage");
            modelAndView.addObject("group", group);
            //set user type
            setUserType(sessionAccount, groupRequest, group, modelAndView);
            //set first n accepted group requests
            List<GroupRequest> someGroupReauests =
                    groupService.getAcceptedGroupRequestsByGroupId(groupId, membersAmountOnGroupPage);
            modelAndView.addObject("members", someGroupReauests);

            //set first n administrators and moderators
            List<GroupRequest> administrators =
                    groupService.getAdminsRequestsByGroupId(groupId, adminAmountOnGroupPage);
            modelAndView.addObject("admins", administrators);

            return modelAndView;

        } catch (Exception e) {
            logger.warn("Exception in method showGroupPage()");
            return new ModelAndView("/personalPage");
        }

    }

    private void setUserType(Account sessionAccount, GroupRequest groupRequest, Group group, ModelAndView modelAndView) {
        if (group.getCreator().getId() == sessionAccount.getId()) {
            modelAndView.addObject("userType", "admin");
        } else if (groupRequest == null) {
            modelAndView.addObject("userType", "unregistered");
        } else if (groupRequest.isAccepted()) {
            if (groupRequest.getAccountType().equals(GroupRequest.AccountInGroupType.USER)) {
                modelAndView.addObject("userType", "user");
            } else if (groupRequest.getAccountType().equals(GroupRequest.AccountInGroupType.MODERATOR)) {
                modelAndView.addObject("userType", "moderator");
            } else if (groupRequest.getAccountType().equals(GroupRequest.AccountInGroupType.ADMIN)) {
                modelAndView.addObject("userType", "admin");
            } else {
                modelAndView.addObject("userType", "not accepted");
            }
        }
    }

    private GroupRequest getGroupRequest(Account account, int groupId) {
        for (GroupRequest groupRequest : account.getGroupRequests()) {
            if (groupRequest.getAccount().getId() == account.getId()) {
                return groupRequest;
            }
        }

        return null;
    }

    @RequestMapping("/groupMembers")
    public ModelAndView showGroupMembers(@RequestParam("id") int groupId, HttpServletRequest req) {
        Account sessionAccount = (Account) req.getSession().getAttribute("account");
        ModelAndView modelAndView = new ModelAndView("/groupMembers");
        try {
            Group group = groupService.getGroupById(groupId);
            List<GroupRequest> groupRequests = groupService.getAcceptedGroupRequestsByGroupId(groupId);
            modelAndView.addObject("members", groupRequests);
            GroupRequest groupRequest = getGroupRequest(sessionAccount, groupId);
            setUserType(sessionAccount, groupRequest, group, modelAndView);
            modelAndView.addObject("membersType", "all group members:");
            modelAndView.addObject("group", group);
        } catch (DaoException e) {
            logger.warn("Exception was thrown in showGroupMembers() with groupId = {}", groupId);
            e.printStackTrace();
        }

        return modelAndView;
    }

    @RequestMapping("/updateGroupMemberType")
    public String updateGroupMemberType(@RequestParam("accountId") int accountId,
                                        @RequestParam("groupId") int groupId,
                                        @RequestParam("type") String type,
                                        @RequestParam("groupRequestId") int groupRequestId) {

        try {
            GroupRequest groupRequest = groupService.getGroupRequstById(groupRequestId);
            groupRequest.setAccountType(GroupRequest.AccountInGroupType.valueOf(type));
            groupService.updateGroupRequest(groupRequest);
        } catch (DaoException e) {
            logger.warn("Exception was thrown in updateGroupMemberType() with parameters:" +
                            " accountId={}, groupId={}, type={}, groupRequestId={}",
                    accountId, groupId, type, groupRequestId);
        }

        return "redirect:/groupMembers?id=" + groupId;
    }

    @RequestMapping("/deleteMemberFromGroup")
    public String deleteMemberFromGroup(@RequestParam("groupId") int groupId,
                                        @RequestParam("groupRequestId") int groupRequestId,
                                        HttpServletRequest request) {
        Account sessionAccount = (Account) request.getSession().getAttribute("account");
        try {
            groupService.removeGroupRequestById(sessionAccount, groupRequestId);
        } catch (DaoException e) {
            logger.warn("Exception was thrown in deleteMemberFromGroup() with parameters:" +
                    " groupRequestId={} groupRequestId);");
            e.printStackTrace();
        }

        return "redirect:/groupMembers?id=?" + groupId;
    }

    @RequestMapping("/acceptGroupRequest")
    public String acceptGroupRequest(@RequestParam("groupId") int groupId,
                                     @RequestParam("groupRequestId") int groupRequestId,
                                     @SessionAttribute("account") Account sessionAccount) {
        try {
            GroupRequest groupRequest = groupService.getGroupRequstById(groupRequestId);
            groupRequest.setAccepted(true);
            groupService.updateGroupRequest(sessionAccount, groupRequest);
        } catch (DaoException e) {
            logger.warn("Exception was thrown in acceptGroupRequest() with parameters:" +
                    " groupId={} groupRequestId={};", groupId, groupRequestId);
            e.printStackTrace();
        }

//        ModelAndView modelAndView = new ModelAndView("groupRequests");
//        modelAndView.addObject("groupId", groupId);
//        return modelAndView;
        return "redirect:/groupMembers?id=?" + groupId;
    }

    @RequestMapping("/showGroupRequests")
    public ModelAndView showGroupRequests(@RequestParam("groupId") int groupId,
                                          @SessionAttribute("account") Account sessionAccount) {

        ModelAndView modelAndView = new ModelAndView("groupRequests");
        try {
            List<GroupRequest> notAcceptedGroupRequests = groupService.getNotAcceptedGroupRequestsByGroupId(groupId);
            Group group = groupService.getGroupById(groupId);

            modelAndView.addObject("group", group);
            modelAndView.addObject("groupRequests", notAcceptedGroupRequests);

        } catch (DaoException e) {
            logger.warn("Exception was thrown in showGroupRequests() with parameters:" +
                    "groupId={}", groupId);
            e.printStackTrace();
        }

        return modelAndView;
    }
}
