<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Update profile</title>
    <script>
        $(function () {
            $("#birthDate").datepicker({dateFormat: 'dd-mm-yy'});
        });
    </script>
</head>
<body>
<form name="updateForm" action="<c:url value="/updateMainInfo"/>" onsubmit="return validateUpdate()" method="post"
      enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-4">
            <table class="table">
                <tr>
                    <td>
                    <td><input type="hidden" name="id" value="${account.id}"/></td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="text" class="form-control" id="name" name="name" value="${account.name}"
                               placeholder="name"/>
                        <span class="alert-warning" id="nameAlert"></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="text" class="form-control" id="surname" name="surname" value="${account.surname}"
                               placeholder="surname"/>
                        <span class="alert-warning" id="surnameAlert"></span>
                    </td>
                </tr>
                <tr>
                    <td><input type="text" class="form-control" id="patronymic" name="patronymic"
                               value="${account.patronymic}" placeholder="patronymic"/></td>
                </tr>
                <tr>
                    <td>
                        <input type="text" class="form-control" id="email" name="email" value="${account.email}"
                               placeholder="email"/>
                        <span class="alert-warning" id="emailAlert"></span>
                    </td>
                </tr>
                <tr>
                    <td><input type="text" class="form-control" name="icq" value="${account.icq}" placeholder="icq"/>
                    </td>
                </tr>
                <tr>
                    <td><input type="text" class="form-control" name="skype" value="${account.skype}"
                               placeholder="skype"/></td>
                </tr>
                <tr>
                    <td><input type="text" class="form-control" name="address" value="${account.address}"
                               placeholder="address"/></td>
                </tr>
                <tr>
                    <td><input type="text" class="form-control" name="birthDate" id="birthDate"
                               value="<fmt:formatDate value='${account.birthDate}' pattern='dd-MM-yyyy'/>"
                               placeholder="birth"/></td>
                </tr>
                <tr>
                    <td><input type="file" class="form-control" name="file"/></td>
                </tr>
            </table>
            <input type="submit" name="update" value="update" class="btn btn-success">
        </div>
    </div>
    <c:choose>
        <c:when test="${not empty param.successUpdate}">
            <c:choose>
                <c:when test="${param.successUpdate.equals('false')}">
                    <span class="alert-danger" id="alertSuccess">update error! check input and try again later.</span>
                </c:when>
                <c:when test="${param.successUpdate.equals('true')}">
                    <span class="alert-success" id="alertSuccess">update success!</span>
                </c:when>
            </c:choose>
        </c:when>
    </c:choose>
</form>
</body>
</html>
