<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
</head>
<body>

<c:if test="${sessionScope.account != null}">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">Social Network</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="<c:url value = "/AllAccountsServlet"/>">all accounts</a></li>
                    <li><a href="<c:url value = "/showAllGroups"/>">all groups</a></li>

                    <form class="navbar-form navbar-left" role="search">
                        <div class="form-group">
                            <input type="text" id="search" class="form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><p class="navbar-text navbar-right">You logged in as <c:out
                            value="${sessionScope.account.name} ${sessionScope.account.surname}   "/></p></li>
                    <li><a href="<c:url value = "/logout"/>"><span class="glyphicon glyphicon-log-out"></span>
                        logout</a></li>

                </ul>
            </div>
        </div>
    </nav>
</c:if>

<script src="<c:url value="/resources/js/ajax.js"/>"></script>
</body>
</html>
