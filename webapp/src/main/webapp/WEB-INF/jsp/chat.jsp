<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" href='<c:url value="/resources/css/chat.css"/>'>

<jsp:include page="/WEB-INF/jsp/personalPageHeader.jsp"/>
<div class="col-md-2">
    <jsp:include page="/WEB-INF/jsp/sideBar.jsp"/>
</div>
<%--<div class="col-md-10">--%>
<div class="container">
    <div class="col-md-12 col-lg-6">
        <div class="panel">
            <!--Heading-->
            <div class="panel-heading">
                <div class="panel-control">
                    <div class="btn-group">
                        <button class="btn btn-default" type="button" data-toggle="collapse" data-target="#demo-chat-body"><i class="fa fa-chevron-down"></i></button>
                        <button type="button" class="btn btn-default" data-toggle="dropdown"><i class="fa fa-gear"></i></button>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="#">Available</a></li>
                            <li><a href="#">Busy</a></li>
                            <li><a href="#">Away</a></li>
                            <li class="divider"></li>
                            <li><a id="demo-connect-chat" href="#" class="disabled-link" data-target="#demo-chat-body">Connect</a></li>
                            <li><a id="demo-disconnect-chat" href="#" data-target="#demo-chat-body">Disconect</a></li>
                        </ul>
                    </div>
                </div>
                <h3 class="panel-title">Chat</h3>
            </div>

            <!--Widget body-->
            <div id="demo-chat-body" class="collapse in">
                <div class="nano has-scrollbar" style="height:75%">
                    <div class="nano-content pad-all" tabindex="0" style="right: -17px;">
                        <ul class="list-unstyled media-block">
                            <c:forEach var = "message" items="${messages}">
                                <c:if test="${message.author.id != sessionScope.account.id}">
                                    <li class="mar-btm">
                                        <div class="media-left">
                                            <%--<img src="<c:url value = "/avatar?id=${account.id}"/>" width="300px" height="auto" class="img-thumbnail"/>--%>
                                            <img src="<c:url value = "/avatar?id=${message.author.id}"/>" class="img-circle img-sm" alt="Profile Picture">
                                        </div>
                                        <div class="media-body pad-hor">
                                            <div class="speech">
                                                <a href="<c:url value="/personalPage?${message.author.id}"/>" class="media-heading">${message.author.name}</a>
                                                <p>${message.text}</p>
                                                <p class="speech-time">
                                                    <i class="fa fa-clock-o fa-fw"></i><fmt:formatDate value='${message.date}' pattern='dd-MM-yy HH:mm'/>
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                </c:if>
                                <c:if test="${message.author.id == sessionScope.account.id}">
                                    <li class="mar-btm">
                                        <div class="media-right">
                                            <img src="<c:url value = "/avatar?id=${message.author.id}"/>" class="img-circle img-sm" alt="Profile Picture">
                                        </div>
                                        <div class="media-body pad-hor speech-right">
                                            <div class="speech">
                                                <a href="<c:url value="/personalPage?${message.author.id}"/>" class="media-heading">${message.author.name}</a>
                                                <p>${message.text}</p>
                                                <p class="speech-time">
                                                    <i class="fa fa-clock-o fa-fw"></i> <fmt:formatDate value='${message.date}' pattern='dd-MM-yy HH:mm'/>
                                                </p>
                                                <a href="#" class="media-bottom media-left">delete</a>
                                            </div>
                                        </div>
                                    </li>
                                </c:if>
                            </c:forEach>
                        </ul>
                    </div>
                    <div class="nano-pane"><div class="nano-slider" style="height: 141px; transform: translate(0px, 0px);"></div></div></div>

                <!--Widget footer-->
                <div class="panel-footer">
                    <div class="row">
                        <form name="sendingMessageForm" action="<c:url value="/sendPrivateMessage"/>" method="post">
                            <input type="hidden" name="receiverId" value="${receiverId}"/>
                            <input type="hidden" name="authorId" value="${sessionScope.account.id}"/>
                            <div class="col-xs-9">
                                <input type="text" name="text" placeholder="Enter your text" class="form-control chat-input">
                            </div>
                            <div class="col-xs-3">
                                <button class="btn btn-primary btn-block" type="submit">Send</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%--</div>--%>
</body>
</html>
