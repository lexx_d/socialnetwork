<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Group</title>
</head>
<body>
<jsp:include page="/WEB-INF/jsp/personalPageHeader.jsp"/>
<div class="col-md-2">
    <jsp:include page="/WEB-INF/jsp/sideBar.jsp"/>
</div>
<c:if test="${userType != null && userType != 'unregistered' && userType != 'not accepted'}">
    <div class="col-md-10">
            <%--    GROUP INFO    --%>
        <div class="row">
            <div class="col-md-3">
                <h3>Group <span class="label label-success">${group.name}</span></h3>
                <img src="<c:url value = "/groupAvatar?id=${group.id}"/>" width="300px" height="auto"
                     class="img-thumbnail"/>
                    <%--<c:if test="${sessionScope.account.id != account.id}">--%>
                    <%--<c:if test="${friendStatus == 'none'}">--%>
                    <%--<form name="updateForm" action="<c:url value="/addFriend"/>" method="post">--%>
                    <%--<input type="hidden" id="id" name="id" value="${account.id}"/>--%>
                    <%--<button class="btn-success">Add to friends</button>--%>
                    <%--</form>--%>
                    <%--</c:if>--%>
                    <%--<c:if test="${friendStatus == 'outgoing'}">--%>
                    <%--<form name="updateForm" action="<c:url value="/deleteFriendRequest"/>" method="post">--%>
                    <%--<input type="hidden" id="id" name="id" value="${account.id}"/>--%>
                    <%--<button class="btn-danger">Withdraw request</button>--%>
                    <%--</form>--%>
                    <%--</c:if>--%>
                    <%--<c:if test="${friendStatus == 'accepted'}">--%>
                    <%--<form name="updateForm" action="<c:url value="/showChat"/>" method="post">--%>
                    <%--<input type="hidden" id="receiverId" name="receiverId" value="${account.id}"/>--%>
                    <%--<input type="hidden" id="authorId" name="authorId" value="${sessionScope.account.id}"/>--%>
                    <%--<button class="btn-primary">Send message</button>--%>
                    <%--</form>--%>
                    <%--</c:if>--%>
                    <%--</c:if>--%>
            </div>

            <div class="col-md-9">
                <div class="row">
                    <br><b>Group Description: </b>
                    <div class="container-fluid" id="groupDescriptionContainer">
                        <div class="row board-container" id="groupRowBoardContainer">
                            <div class="board" id="groupDescriptionBoard">${group.description}</div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="label-info">
                        administration:
                    </div>
                    <div class="col-md-2">
                        <div class="row">
                            <img src="<c:url value="/avatar?id=${group.creator.id}" />"
                                 class="img-circle media-object" width="64" height="64" alt="Profile Picture">
                        </div>
                        <div class="row">
                            <a href="<c:url value="/personalPage?id=${group.creator.id}"/>">creator</a>
                        </div>
                    </div>
                    <c:forEach var="userRequest" items="${admins}">
                        <div class="col-md-2">
                            <div class="row">
                                <img src="<c:url value="/avatar?id=${userRequest.account.id}" />"
                                     class="img-circle media-object" width="64" height="64" alt="Profile Picture">
                            </div>
                            <div class="row">
                                <a href="<c:url value="/personalPage?id=${userRequest.account.id}"/>">
                                        ${userRequest.accountType.toString()}
                                </a>
                            </div>
                        </div>
                    </c:forEach>
                </div>

                <div class="row">
                    <div class="label-info" href="#">
                        <a href="<c:url value="/groupMembers?id=${group.id}"/>">users:</a>
                    </div>
                    <c:forEach var="userRequest" items="${members}">
                        <div class="col-md-2">
                            <div class="row">
                                <img src="<c:url value="/avatar?id=${userRequest.account.id}" />"
                                     class="img-circle media-object" width="64" height="64" alt="Profile Picture">
                            </div>
                            <div class="row">
                                <a href="<c:url value="/personalPage?id=${userRequest.account.id}"/>">
                                        ${userRequest.account.name}
                                </a>
                            </div>
                        </div>
                    </c:forEach>
                </div>

                <br>
                <c:if test="${userType == 'admin'}">
                    <div class="btn-group">
                        <form name="updateForm" action="<c:url value="/showChat"/>" method="post">
                            <button class="btn btn-primary">
                                edit group info
                            </button>
                        </form>
                        <form name="updateForm1" action="<c:url value="/showChat1"/>" method="post">
                            <button class="btn btn-primary">
                                show group requests
                            </button>
                        </form>
                    </div>
                </c:if>
            </div>
        </div>

            <%--   WALL   --%>
        <div class="row">
            <div class="col-md-5">
                <div class="row">
                    <form id="addWallMessage" name="addWallMessage" action="<c:url value="/addWallMessage"/>"
                          method="post">
                        <input type="hidden" id="accountId" name="accountId" value="${account.id}"/>
                        <textarea name="newWallMessage" class="text-area" rows="2" style="min-width: 100%"></textarea>
                        <input type="submit" name="sendMessage" value="Send message" class="btn btn-primary">
                    </form>
                </div>
                <div class="row">
                    <div class="jumbotron" id="wall"></div>
                </div>
            </div>
        </div>
            <%--   PAGINATION   --%>
        <div class="row">
            <div class="col-md-3">
                <nav aria-label="Page navigation">
                    <ul class="pagination">
                        <li>
                            <a href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        <c:forEach begin="1" end="${wallPageCount}" varStatus="loopIndex">
                            <li title="${loopIndex.index}"><a href="">${loopIndex.index}</a></li>
                        </c:forEach>
                        <li>
                            <a href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    </div>
</c:if>
<script src="<c:url value="/resources/js/pagination.js"/>"></script>
<link rel="stylesheet" href="<c:url value="/resources/css/grouppage.css"/>">
<%--<link rel="stylesheet" href='<c:url value="/resources/css/chat.css"/>'>--%>
</body>
</html>
