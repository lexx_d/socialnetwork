<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>All people</title>
</head>
<body>
<jsp:include page="/WEB-INF/jsp/personalPageHeader.jsp"/>
<div class="row">
    <div class="col-md-2">
        <jsp:include page="/WEB-INF/jsp/sideBar.jsp"/>
    </div>
    <div class="col-md-10">
        <div class="ui-widget">
            <input id="searchAccounts" placeholder="start typing">
            <button id="btnSearchAccounts">search</button>
        </div>
        <b>All people:</b><br>
        <table class="table table-hover">
            <c:forEach var="account" items="${accounts}">
                <tr class='clickable-row' data-href='<c:url value='/personalPage?id=${account.id}'/>'>
                    <td>
                        <img src="<c:url value = "/avatar?id=${account.id}"/>" width="70px" height="auto"/>
                    </td>
                    <td>${account.name}</td>
                    <td>${account.surname}</td>
                    <td>${account.email}</td>
                    <td>${account.skype}</td>
                </tr>
            </c:forEach>
        </table>
    </div>
</div>

<script>
    jQuery(document).ready(function ($) {
        $(".clickable-row").click(function () {
            window.document.location = $(this).data("href");
        });
    });
</script>
</body>
</html>
