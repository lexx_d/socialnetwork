<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Update password</title>
</head>
<body>
<form name="updatePasswordForm" action="<c:url value="/updatePassword"/>" onsubmit="return validatePassword()"
      method="post">

    <div class="col-md-4">
        <div class="input-group">
            <label for="pswd1">Enter new password:</label>
            <input type="password" class="form-control" id="pswd1" name="pswd1">
        </div>
        <div class="input-group">
            <label for="pswd2">Confirm password:</label>
            <input type="password" class="form-control" id="pswd2" name="pswd2">
        </div>
        <div class="input-group">
            <input type="submit" name="updatPassword" value="Update password" class="btn-primary"/>
        </div>
    </div>

</form>
</body>
</html>
