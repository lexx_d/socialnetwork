<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Creating group</title>
</head>
<body>
<jsp:include page="/WEB-INF/jsp/personalPageHeader.jsp"/>
<div class="col-md-2">
    <jsp:include page="/WEB-INF/jsp/sideBar.jsp"/>
</div>
<div class="col-md-10">
    <form name="createGroupForm" action="<c:url value="/createGroup"/>" onsubmit="return validateGroupCreation()" method="post"
          enctype="multipart/form-data">
        <div class="row">
            <div class="col-md-4">
                <table class="table">
                    <tr>
                        <td><input type="hidden" name="id" value="${account.id}"/></td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" class="form-control" id="groupName" name="groupName" value=""
                                   placeholder="group name"/>
                            <span class="alert-warning" id="groupNameAlert"></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <textarea class="form-control" id="description" name="description"
                                   placeholder="description..." rows="10"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td><input type="file" class="form-control" name="file"/></td>
                    </tr>
                </table>
                <input type="submit" name="createNewGroup" value="create group" class="btn btn-success">
            </div>
        </div>
        <c:choose>
            <c:when test="${not empty param.successUpdate}">
                <c:choose>
                    <c:when test="${param.successUpdate.equals('false')}">
                        <span class="alert-danger" id="alertSuccess">update error! check input and try again later.</span>
                    </c:when>
                    <c:when test="${param.successUpdate.equals('true')}">
                        <span class="alert-success" id="alertSuccess">update success!</span>
                    </c:when>
                </c:choose>
            </c:when>
        </c:choose>
    </form>
</div>
</body>
</html>
