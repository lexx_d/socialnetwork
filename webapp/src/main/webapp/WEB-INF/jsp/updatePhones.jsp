<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Update Phones</title>
</head>
<body>
<form name="updateForm" action="<c:url value="/updatePhones"/>" onsubmit="tableToArray();return validateUpdate()"
      method="post">
    <div class="row">
        <div class="col-md-6">
            <br> Phones:
            <table id="phones" class="table">
                <c:forEach var="phone" items="${sessionScope.account.phones}">
                    <tr>
                        <td>${phone.name}</td>
                        <td>${phone.number}</td>
                        <td>
                            <button type="button" onclick="deleteRow(this.parentElement)" class="btn btn-danger"><span
                                    class="glyphicon glyphicon-remove"></span></button>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <label for="newPhoneName">Name:</label>
            <input type="text" class="form-control" id="newPhoneName" value="" width="150"/>
        </div>
        <div class="col-md-3">
            <label for="newPhone">Phone number:</label>
            <input type="text" class="form-control" id="newPhone" name="newPhone" value="" width="150"/>
        </div>
        <div class="col-md-1">
            <br>
            <button type="button" onclick="addRow()" class="btn btn-success"><span
                    class="glyphicon glyphicon-plus"></span></button>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-1">
            <input type="hidden" id="phonesArray" name="phonesArray" value=""/>
            <input type="submit" value="update" class="btn btn-success">
        </div>
    </div>
</form>
</body>
</html>
