<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="<c:url value='resources/css/bootstrap.min.css'/>"/>
    <%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>--%>
    <script src="<c:url value='resources/js/bootstrap.min.js'/>"></script>
    <script src="<c:url value="/resources/js/validation.js"/>"></script>
    <script src="<c:url value="/resources/js/phoneValidation.js"/>"></script>
</head>
<body>
<jsp:include page="/WEB-INF/jsp/personalPageHeader.jsp"/>
<div class="row">
    <div class="col-md-2">
        <jsp:include page="/WEB-INF/jsp/sideBar.jsp"/>
    </div>
    <div class="col-md-8">

        <%--<ul class="nav nav-tabs tabs-up" id="tabs">--%>
        <%--<li class="active"><a href="<c:url value = "/showUpdateMainInfo"/>" data-target="#mainInfo" class="media_node active span"--%>
        <%--id="mainInfoTab" data-toggle="tabajax" rel="tooltip"> Main info </a></li>--%>
        <%--<li><a href="<c:url value = "/showUpdatePhones"/>" data-target="#updatePhones" class="media_node span"--%>
        <%--id="updatePhonesTab" data-toggle="tabajax" rel="tooltip"> Update phones</a></li>--%>
        <%--<li><a href="<c:url value = "/showUpdatePassword"/>" data-target="#updatePassword" class="media_node span"--%>
        <%--id="updatePasswordTab" data-toggle="tabajax" rel="tooltip">Update password</a></li>--%>
        <%--</ul>--%>

        <%--<div class="tab-content">--%>
        <%--<div class="tab-pane active" id="mainInfo">--%>
        <%--</div>--%>
        <%--<div class="tab-pane" id="updatePhones">--%>
        <%--</div>--%>
        <%--<div class="tab-pane  urlbox span8" id="updatePassword">--%>
        <%--</div>--%>
        <%--</div>--%>

        <ul class="nav nav-tabs tabs-up" id="UpdateTabs">
            <li class="active"><a href="#mainInfo" data-url="<c:url value = "/showUpdateMainInfo"/>">Main info</a></li>
            <li><a href="#phonesTab" data-url="/embed/62806/view">Phones</a></li>
            <li><a href="#passwordTab" data-url="/embed/62807/view">Password</a></li>
        </ul>

        <div class="tab-content">
            <%--<div class="tab-pane active" id="mainInfo"><jsp:include page="/WEB-INF/jsp/updateMainInfo.jsp"/></div>--%>
            <div class="tab-pane active" id="mainInfo">
                <jsp:include page="/showUpdateMainInfo"/>
            </div>
            <div class="tab-pane" id="phonesTab">
                <jsp:include page="/WEB-INF/jsp/updatePhones.jsp"/>
            </div>
            <div class="tab-pane" id="passwordTab">
                <jsp:include page="/WEB-INF/jsp/updatePassword.jsp"/>
            </div>
        </div>
    </div>
</div>

<script>
    //    $('[data-toggle="tabajax"]').click(function(e) {
    //        e.preventDefault();
    //        var $this = $(this),
    //                loadurl = $this.attr('href'),
    //                targ = $this.attr('data-target');
    //
    //        $.get(loadurl, function(data) {
    //            $(targ).html(data);
    //        });
    //
    //        $this.tab('show');
    //    });

    //    // load first tab content
    //    $('#mainInfo').load($('.active a').attr("href"),function(result){
    //        $('.active a').tab('show');
    //    });

    $('#UpdateTabs a').click(function (e) {
        e.preventDefault();

        var url = $(this).attr("data-url");
        var href = this.hash;
        var pane = $(this);

//    // ajax load from data-url
//    $(href).load(url,function(result){
//        pane.tab('show');
//    });

        pane.tab('show');
    });

    //// load first tab content
    //$('#home').load($('.active a').attr("data-url"),function(result){
    //    var url = $(this).attr("data-url");
    //    var href = this.hash;
    //    var pane = $(this);
    //    $(href).load(url,function(result){
    //        pane.tab('show');
    //    });
    //    //$('.active a').tab('show');
    //});
</script>

</body>
</html>
