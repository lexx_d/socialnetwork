<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="/WEB-INF/jsp/personalPageHeader.jsp"/>
<div class="row">
    <div class="col-md-2">
        <jsp:include page="/WEB-INF/jsp/sideBar.jsp"/>
    </div>
    <div class="col-md-10">
        <%--<div class="ui-widget">--%>
            <%--<input id="searchAccounts" placeholder="start typing">--%>
            <%--<button id="btnSearchAccounts">search</button>--%>
        <%--</div>--%>
        <button class="btn-primary" href="<c:url value="/groupPage?id=${group.id}"/> ">Back to group</button>
        <br>
        <b>${membersType}</b>
        <br>
        <table class="table table-hover">
            <c:forEach var="request" items="${members}">
                <tr class='clickable-row' data-href='<c:url value='/personalPage?id=${request.account.id}'/>'>
                    <td>
                        <img src="<c:url value = "/avatar?id=${request.account.id}"/>" width="70px" height="auto"/>
                    </td>
                    <td>${request.account.name}</td>
                    <td>${request.account.surname}</td>
                    <c:if test="${userType == 'admin'}">
                        <td>
                            <form action="<c:url value = "/deleteMemberFromGroup"/>" method="post">
                                <input type="hidden" name="accountId" value="${request.account.id}">
                                <input type="hidden" name="groupId" value="${group.id}">
                                <input type="hidden" name="groupRequestId" value="${request.id}">
                                <button class="btn-danger">delete</button>
                            </form>
                        </td>
                        <td>
                            <c:choose>
                                <c:when test="${request.accountType == 'ADMIN'}">
                                    <form action="<c:url value = "/updateGroupMemberType"/>" method="post">
                                        <input type="hidden" name="accountId" value="${request.account.id}">
                                        <input type="hidden" name="groupId" value="${group.id}">
                                        <input type="hidden" name="groupRequestId" value="${request.id}">
                                        <input type="hidden" name="type" value="USER">
                                        <button class="btn-danger">make user</button>
                                    </form>
                                </c:when>
                                <c:when test="${request.accountType != 'ADMIN'}">
                                    <form action="<c:url value = "/updateGroupMemberType"/>" method="post">
                                        <input type="hidden" name="accountId" value="${request.account.id}">
                                        <input type="hidden" name="groupId" value="${group.id}">
                                        <input type="hidden" name="groupRequestId" value="${request.id}">
                                        <input type="hidden" name="type" value="ADMIN">
                                        <button class="btn-success">make admin</button>
                                    </form>
                                </c:when>
                            </c:choose>
                        </td>
                        <td>
                            <c:choose>
                                <c:when test="${request.accountType == 'MODERATOR'}">
                                    <form action="<c:url value = "/updateGroupMemberType"/>" method="post">
                                        <input type="hidden" name="accountId" value="${request.account.id}">
                                        <input type="hidden" name="groupId" value="${group.id}">
                                        <input type="hidden" name="groupRequestId" value="${request.id}">
                                        <input type="hidden" name="type" value="USER">
                                        <button class="btn-danger">make user</button>
                                    </form>
                                </c:when>
                                <c:when test="${request.accountType != 'MODERATOR'}">
                                    <form action="<c:url value = "/updateGroupMemberType"/>" method="post">
                                        <input type="hidden" name="accountId" value="${request.account.id}">
                                        <input type="hidden" name="groupId" value="${group.id}">
                                        <input type="hidden" name="groupRequestId" value="${request.id}">
                                        <input type="hidden" name="type" value="MODERATOR">
                                        <button class="btn-success">make moderator</button>
                                    </form>
                                </c:when>
                            </c:choose>
                        </td>
                    </c:if>
                </tr>
            </c:forEach>
        </table>
    </div>
</div>

<script>
    jQuery(document).ready(function ($) {
        $(".clickable-row").click(function () {
            window.document.location = $(this).data("href");
        });
    });
</script>
