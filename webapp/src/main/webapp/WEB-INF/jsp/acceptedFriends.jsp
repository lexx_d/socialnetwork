<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Accepted friends</title>
</head>
<body>
<div class="col-md-6">
    <b>${account.getName()} friends:</b><br>
    <table class="table table-hover">
        <c:forEach var="friend" items="${friends}">
            <%--<a href="<c:url value='/personalPage?id=${friend.id}'/>">--%>
            <tr class='clickable-row' data-href='<c:url value='/personalPage?id=${friend.id}'/>'>
                <td>
                    <img src="<c:url value = "/avatar?id=${friend.id}"/>" width="70px" height="auto"/>
                </td>
                <td class="active">${friend.name}</td>
                <td>${friend.surname}</td>
                <td>${friend.email}</td>
                <td>${friend.skype}</td>
                <td>
                    <form action="<c:url value = "/deleteFriendRequest"/>" method="post">
                        <input type="hidden" name="id" value="${friend.id}">
                        <button class="btn btn-danger">decline</button>
                    </form>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
<script>
    jQuery(document).ready(function ($) {
        $(".clickable-row").click(function () {
            window.document.location = $(this).data("href");
        });
    });
</script>
</body>
</html>
