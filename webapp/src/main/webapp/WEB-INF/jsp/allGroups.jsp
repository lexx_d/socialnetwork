<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Groups</title>
</head>
<body>
<jsp:include page="/WEB-INF/jsp/personalPageHeader.jsp"/>
<div class="row">
    <div class="col-md-2">
        <jsp:include page="/WEB-INF/jsp/sideBar.jsp"/>
    </div>
    <div class="col-md-7">
        <div class="ui-widget">
            <input id="searchGroups" class="text-input" placeholder="start typing">
            <button id="btnSearchAccounts" class="btn-default">search</button>
        </div>
        <b>All groups:</b><br>
        <table class="table table-hover">
            <c:forEach var="group" items="${groups}">
                <tr class='clickable-row' data-href='<c:url value='/groupPage?id=${group.id}'/>'>
                    <td>
                        <img src="<c:url value = "/groupAvatar?id=${group.id}"/>" width="70px" height="auto"/>
                    </td>
                    <td>${group.name}</td>
                </tr>
            </c:forEach>
        </table>
    </div>
    <div class="col-md-3">
        <form name="createGroupBtnForm" action="<c:url value="/showGroupCreatingForm"/>">
            <input type="hidden" name="creatorId" value="${sessionScope.account.id}"/>
            <button type="submit" class="btn-primary">create group</button>
        </form>
    </div>
</div>

<script>
    jQuery(document).ready(function ($) {
        $(".clickable-row").click(function () {
            window.document.location = $(this).data("href");
        });
    });
</script>

</body>
</html>
