<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="<c:url value='resources/css/bootstrap.min.css'/>"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="<c:url value='resources/js/bootstrap.min.js'/>"></script>
    <script src="<c:url value="/resources/js/validation.js"/>"></script>
    <script src="<c:url value="/resources/js/phoneValidation.js"/>"></script>
    <script>
        $(function () {
            $("#birthDate").datepicker();
        });
    </script>
    <title>Registration page</title>
</head>
<body>
<%--<jsp:include page="/WEB-INF/jsp/personalPageHeader.jsp"/>--%>
<div class="row">
    <c:choose>
        <c:when test="${sessionScope.account != null}">
        <div class="col-md-2">
            <jsp:include page="/WEB-INF/jsp/sideBar.jsp"/>
        </div>
        <div class="col-md-10">
            <form name="updateForm" action="/updateServlet" onsubmit="tableToArray();return validateUpdate()" method="post"
                  enctype="multipart/form-data">
                    <c:set var="acceptBtnName" value="save" scope="page"/>
        </c:when>
                    <%--  REGISTRATION PAGE  --%>
        <c:when test="${sessionScope.account == null}">
        <div class="col-md-12">
            <form name="updateForm" action="/registrationServlet" onsubmit="tableToArray();return validateUpdate()"
                  method="post">
                <c:set var="acceptBtnName" value="register" scope="page"/>
        </c:when>
    </c:choose>
    <table>
        <tr>
            <td>
            <td><input type="hidden" name="id" value="${account.id}"/></td>
            </td>
        </tr>
        <tr>
            <td>
                <input type="text" id="name" name="name" value="${account.name}" placeholder="name"/>
                <span class="alert-warning" id="nameAlert"></span>
            </td>
        </tr>
        <tr>
            <td>
                <input type="text" id="surname" name="surname" value="${account.surname}"
                       placeholder="surname"/>
                <span class="alert-warning" id="surnameAlert"></span>
            </td>
        </tr>
        <tr>
            <td><input type="text" id="patronymic" name="patronymic" value="${account.patronymic}"
                       placeholder="patronymic"/></td>
        </tr>
        <tr>
            <td>
                <input type="text" id="email" name="email" value="${account.email}"
                       placeholder="email"/>
                <span class="alert-warning" id="emailAlert"></span>
            </td>
        </tr>
        <tr>
            <td><input type="text" name="icq" value="${account.icq}" placeholder="icq"/></td>
        </tr>
        <tr>
            <td><input type="text" name="skype" value="${account.skype}" placeholder="skype"/></td>
        </tr>
        <tr>
            <td><input type="text" name="address" value="${account.address}" placeholder="address"/>
            </td>
        </tr>
        <tr>
            <td><input type="text" name="birthDate" id="birthDate" value="${account.birthDate}"
                       placeholder="birth"/></td>
        </tr>
        <tr>
            <td><input type="password" name="password" value="${account.password}"
                       placeholder="password"/></td>
        </tr>
        <tr>
            <td><input type="file" name="file"/></td>
        </tr>
    </table>

                    <%--<div class="11container">--%>
                    <br> Phones:
                    <table id="phones">
                        <c:forEach var="phone" items="${account.phones}">
                            <tr>
                                <td>${phone.name}</td>
                                <td>${phone.number}</td>
                                <td>
                                    <button type="button" onclick="deleteRow(this.parentElement)">-</button>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                    <br> Enter new phone: <br>
                    name: <input type="text" id="newPhoneName" value="" width="150"/>
                    phone number: <input type="text" id="newPhone" name="newPhone" value="" width="150"/>
                    <button type="button" onclick="addRow()">+</button>
                    <%--</div>--%>
                    <br>
                    <input type="hidden" id="phonesArray" name="phonesArray" value=""/>
                    <input type="submit" name="register" value="${acceptBtnName}">

                    <c:choose>
                        <c:when test="${not empty param.successUpdate}">
                            <c:choose>
                                <c:when test="${param.successUpdate.equals('false')}">
                                    update error! check input and try again later.
                                </c:when>
                                <c:when test="${param.successUpdate.equals('true')}">
                                    update success!
                                </c:when>
                            </c:choose>
                        </c:when>
                    </c:choose>
                </form>
            </div>
</body>
</html>
