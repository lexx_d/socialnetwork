<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Friends</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <%--<script src="https://code.jquery.com/jquery-1.12.4.js"></script>--%>
    <%--<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>--%>
    <%--<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>--%>
    <%--<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">--%>
</head>
<body>
<jsp:include page="/WEB-INF/jsp/personalPageHeader.jsp"/>
<div class="row">
    <div class="col-md-2">
        <jsp:include page="/WEB-INF/jsp/sideBar.jsp"/>
    </div>
    <div class="col-md-6">
        <ul class="nav nav-tabs tabs-up" id="friendsTabs">
            <li class="active"><a href="#friends" data-url="<c:url value = "/showUpdateMainInfo"/>">Friends</a></li>
            <li><a href="#inFriends" data-url="/embed/62806/view">Incoming requests</a></li>
            <li><a href="#outFriends" data-url="/embed/62807/view">Outgoing requests</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="friends">
                <jsp:include page="/showAcceptedFriends"/>
            </div>
            <div class="tab-pane" id="inFriends">
                <jsp:include page="/showIncomingFriends"/>
            </div>
            <div class="tab-pane" id="outFriends">
                <jsp:include page="/showOutgoingFriends"/>
            </div>
        </div>
    </div>

    <script>
        jQuery(document).ready(function ($) {
            $(".clickable-row").click(function () {
                window.document.location = $(this).data("href");
            });
        });

        $('#friendsTabs a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        });
    </script>
</div>
</body>
</html>
