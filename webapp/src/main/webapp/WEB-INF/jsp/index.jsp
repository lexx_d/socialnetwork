<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html;charset=UTF-8" language="java" %>
<%--<%@page session="false" %>--%>

<html>
<head>
    <script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
    <%--<script src="${pageContext.request.contextPath}/resources/css/bootstrap.min.css"></script>--%>
    <%--<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">--%>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link type="text/css" rel="stylesheet" href="<spring:url value="/resources/css/bootstrap.min.css"/>"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <%--<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">--%>
    <%--<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>--%>
</head>
<body>

<%-- response.sendRedirect(request.getContextPath() + "/AllAccountsServlet"); --%>
<%--<div class="container">--%>
<%--<h2>Authorization</h2>--%>
<%--<form class="form-horizontal" role="form" action="<c:url value = "/loginChecker"/>" method="post">--%>
<%--<div class="form-group">--%>
<%--<label class="control-label col-sm-2" for="email">email:</label>--%>
<%--<div class="col-sm-10">--%>
<%--<input type="email" name="email" id="email" value=""/><br>--%>
<%--</div>--%>
<%--</div>--%>
<%--<div class="form-group">--%>
<%--<label class="control-label col-sm-2" for="pwd">password:</label>--%>
<%--<div class="col-sm-10">--%>
<%--<input type="password" name="password" id="pwd" value=""/><br>--%>
<%--</div>--%>
<%--</div>--%>
<%--<div class="form-group">--%>
<%--<div class="col-sm-offset-2 col-sm-10">--%>
<%--<div class="checkbox">--%>
<%--<label><input type="checkbox" id="rememberMe" name="rememberMe" checked> remember me</label>--%>
<%--</div>--%>
<%--</div>--%>
<%--</div>--%>
<%--<div class="form-group">--%>
<%--<div class="col-sm-offset-2 col-sm-10">--%>
<%--<button type=submit name="login" class="btn btn-default">login</button>--%>
<%--</div>--%>
<%--</div>--%>
<%--<c:if test="${param.login.equals('error')}">--%>
<%--<div class="form-group">--%>
<%--<div class="col-sm-offset-2 col-sm-10">--%>
<%--<span class="alert-warning" id="auth">authorization error! try again</span>--%>
<%--</div>--%>
<%--</div>--%>
<%--</c:if>--%>
<%--<br>--%>
<%--<div class="form-group">--%>
<%--<div class="col-sm-offset-2 col-sm-10">--%>
<%--<a href="<c:url value = "/accountRegistration"/>"> registration </a>--%>
<%--</div>--%>
<%--</div>--%>
<%--&lt;%&ndash; request.getRequestDispatcher(request.getContextPath() + "/AllAccountsServlet").forward(request, response); &ndash;%&gt;--%>
<%--</form>--%>
<%--</div>--%>

<div id="loginModal" class="modal show" tabindex="-1" role="dialog" aria-hidden="true">
    <form class="form-horizontal" role="form" action="<c:url value = "/loginChecker"/>" method="post">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--%>
                    <h1 class="text-center">Authorization</h1>
                </div>
                <div class="modal-body">
                    <form class="form col-md-12 center-block">
                        <div class="form-group">
                            <input type="text" class="form-control input-lg" name="email" id="email"
                                   placeholder="Email">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control input-lg" name="password" id="pwd"
                                   placeholder="Password">
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-0 col-sm-10">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="rememberMe" name="rememberMe" checked> Remember me
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary btn-lg btn-block" type="submit">Sign In</button>
                            <span class="pull-right" style="font-size: medium"><a
                                    href="<c:url value = "/accountRegistration"/>">Registration</a></span>
                        </div>
                    </form>
                </div>
                <%--<div class="modal-footer">--%>
                <%--<div class="col-md-12">--%>
                <%--<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>--%>
                <%--</div>--%>
                <%--</div>--%>
            </div>
        </div>
    </form>
</div>
</body>
</html>
