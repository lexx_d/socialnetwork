<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<c:if test="${sessionScope.account != null}">
<ul class="nav nav-pills nav-stacked">
    <li class="active">><a href="<c:url value = "/personalPage?id=${sessionScope.account.id}"/>">my page</a></li>
    <li><a href="<c:url value = "/${sessionScope.account.id}/friends"/>">my friends</a></li>
    <li><a href="NotDeveloped.jsp">my groups</a></li>
    <%--<li><a href="<c:url value = "/chat"/>">messages</a></li>--%>
    <%--<li><a href="NotDeveloped.jsp">my photos</a></li>--%>
    <li><a href="<c:url value = "/showUpdateInfo"/>">update my profile</a></li>
</ul>
</c:if>

<body>
