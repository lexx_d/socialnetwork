<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="row">
    <div class="col-md-2">
        <jsp:include page="/WEB-INF/jsp/sideBar.jsp"/>
    </div>
    <div class="col-md-10">
        <button class="btn-primary" href="<c:url value="/groupPage?id=${group.id}"/> ">Back to group</button><br>
        <b>requests:</b><br>
        <table class="table table-hover">
            <c:forEach var="request" items="${groupRequests}">
                <tr class='clickable-row' data-href='<c:url value='/personalPage?id=${request.account.id}'/>'>
                    <td>
                        <img src="<c:url value = "/avatar?id=${request.account.id}"/>" width="70px" height="auto"/>
                    </td>
                    <td>${request.account.name}</td>
                    <td>${request.account.surname}</td>
                    <td>
                        <form action="<c:url value = "/acceptGroupRequest"/>" method="post">
                            <input type="hidden" name="accountId" value="${request.account.id}">
                            <input type="hidden" name="groupId" value="${group.id}">
                            <input type="hidden" name="groupRequestId" value="${request.id}">
                            <button class="btn-success">accept</button>
                        </form>
                    </td>
                    <td>
                        <form action="<c:url value = "/deleteMemberFromGroup"/>" method="post">
                            <input type="hidden" name="accountId" value="${request.account.id}">
                            <input type="hidden" name="groupId" value="${group.id}">
                            <input type="hidden" name="groupRequestId" value="${request.id}">
                            <button class="btn-danger">decline</button>
                        </form>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>
</div>

<script>
    jQuery(document).ready(function ($) {
        $(".clickable-row").click(function () {
            window.document.location = $(this).data("href");
        });
    });
</script>

