<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Incoming friend requests</title>
</head>
<body>
<label for="inFriendRequests">Incoming friend requests:</label>
<table id="inFriendRequests" class="table table-hover">
    <c:forEach var="friend" items="${incomingFriends}">
        <tr class='clickable-row' data-href='<c:url value='/personalPage?id=${friend.id}'/>'>
            <td>
                <img src="<c:url value = "/avatar?id=${friend.id}"/>" width="70px" height="auto"/>
            </td>
            <td class="active">${friend.name}</td>
            <td>${friend.surname}</td>
            <td>${friend.email}</td>
            <td>${friend.skype}</td>
            <td>
                <form action="<c:url value = "/acceptFriendRequest"/>" method="post">
                    <input type="hidden" name="id" value="${friend.id}">
                    <button class="btn btn-success">accept</button>
                </form>
            </td>
            <td>
                <form action="<c:url value = "/deleteFriendRequest"/>" method="post">
                    <input type="hidden" name="id" value="${friend.id}">
                    <button class="btn btn-danger">decline</button>
                </form>
            </td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
