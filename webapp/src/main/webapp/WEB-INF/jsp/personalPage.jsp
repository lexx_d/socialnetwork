<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>Account page</title>
</head>
<body>
<jsp:include page="/WEB-INF/jsp/personalPageHeader.jsp"/>
<div class="col-md-2">
    <jsp:include page="/WEB-INF/jsp/sideBar.jsp"/>
</div>
<div class="col-md-10">
    <%--    ACCOUNT INFO    --%>
    <div class="row">
        <div class="col-md-3">
            <img src="<c:url value = "/avatar?id=${account.id}"/>" width="300px" height="auto" class="img-thumbnail"/>
            <c:if test="${sessionScope.account.id != account.id}">
                <c:if test="${friendStatus == 'none'}">
                    <form name="updateForm" action="<c:url value="/addFriend"/>" method="post">
                        <input type="hidden" id="id" name="id" value="${account.id}"/>
                        <button class="btn-success">Add to friends</button>
                    </form>
                </c:if>
                <c:if test="${friendStatus == 'outgoing'}">
                    <form name="updateForm" action="<c:url value="/deleteFriendRequest"/>" method="post">
                        <input type="hidden" id="id" name="id" value="${account.id}"/>
                        <button class="btn-danger">Withdraw request</button>
                    </form>
                </c:if>
                <c:if test="${friendStatus == 'accepted'}">
                    <form name="updateForm" action="<c:url value="/showChat"/>" method="post">
                        <input type="hidden" id="receiverId" name="receiverId" value="${account.id}"/>
                        <input type="hidden" id="authorId" name="authorId" value="${sessionScope.account.id}"/>
                        <button class="btn-primary">Send message</button>
                    </form>
                </c:if>
            </c:if>
        </div>
        <div class="col-md-5">
            <br><b>Personal info</b>
            <table class="table">
                <tr>
                    <td>${account.name}</td>
                </tr>
                <tr>
                    <td>${account.surname}</td>
                </tr>
                <tr>
                    <td>${account.email}</td>
                </tr>
                <tr>
                    <td>${account.skype}</td>
                </tr>
                <tr>
                    <td><fmt:formatDate value='${account.birthDate}' pattern='dd-MM-yyyy'/></td>
                </tr>
            </table>
            <label for="phones">Phones:</label>
            <table id="phones" class="table">
                <c:forEach var="phone" items="${account.phones}">
                    <tr>
                        <td>${phone.name}</td>
                        <td>${phone.number}</td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </div>
    <%--   WALL   --%>
    <div class="row">
        <div class="col-md-5">
            <div class="row">
                <form id="addWallMessage" name="addWallMessage" action="<c:url value="/addWallMessage"/>" method="post">
                    <input type="hidden" id="accountId" name="accountId" value="${account.id}"/>
                    <textarea name="newWallMessage" class="text-area" rows="2" style="min-width: 100%"></textarea>
                    <input type="submit" name="sendMessage" value="Send message" class="btn btn-primary">
                </form>
            </div>
            <div class="row">
                <div class="jumbotron" id="wall"></div>
            </div>
        </div>
    </div>
    <%--   PAGINATION   --%>
    <div class="row">
        <div class="col-md-3">
            <nav aria-label="Page navigation">
                <ul class="pagination">
                    <li>
                        <a href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                    <c:forEach begin="1" end="${wallPageCount}" varStatus="loopIndex">
                        <li title="${loopIndex.index}"><a href="">${loopIndex.index}</a></li>
                    </c:forEach>
                    <li>
                        <a href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>
<script src="<c:url value="/resources/js/pagination.js"/>"></script>
</body>
</html>
