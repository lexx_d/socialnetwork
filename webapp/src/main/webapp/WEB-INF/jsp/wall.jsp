<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<link rel="stylesheet" href='<c:url value='/resources/css/socialnet.css'/>'>
<table class="table">
<c:forEach var="message" items="${messages}">
    <tr class='clickable-row' data-href='<c:url value='/personalPage?id=${message.author.id}'/>'>
        <td>
            <img src="<c:url value = "/avatar?id=${message.author.id}"/>" width="70px" height="auto"/>
            ${message.author.surname} ${message.author.name} :
        </td>
    </tr>
    <tr>
        <td>
            <textarea readonly class="text-field" rows="3" style="min-width: 100%">
                ${message.text}
            </textarea>
            <br>
            <%-- author or wall owner can delete message --%>
            <c:if test="${sessionScope.account.id == message.author.id || sessionScope.account.id == message.accountReceiver.id}">
                <a href='<c:url value='/deleteWallMessage/${message.id}/${message.accountReceiver.id}'/>'>delete</a>
            </c:if>
        </td>
    </tr>
</c:forEach>
</table>

<script>
    jQuery(document).ready(function ($) {
        $(".clickable-row").click(function () {
            window.document.location = $(this).data("href");
        });
    });
</script>

