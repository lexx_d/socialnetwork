$(function () {
    $("#search").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: '/showAccounts', //'<c:url value="/showAccounts"/>',
                data: {
                    filter: request.term
                },
                success: function (data) {
                    response($.map(data, function (account, i) {
                        return {
                            id: account.id,
                            value: account.name + ' ' + account.surname,
                            label: account.name + ' ' + account.surname
                        }
                    }));
                    // Handle 'no match' indicated by [ "" ] response
                    //response(data.length === 1 && data[0].length === 0 ? [] : data);
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            location.href = "/personalPage?id=" + ui.item.id;
        }
    });
});