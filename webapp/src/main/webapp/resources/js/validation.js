function validateUpdate() {
    var nameAlert = $("#nameAlert");
    var surnameAlert = $("#surnameAlert");
    var emailAlert = $("#emailAlert");
    nameAlert.html("");
    surnameAlert.html("");
    emailAlert.html("");
    var isPassed = true;
    var name = document.forms["updateForm"]["name"].value;
    //var name = $("form[name='updateFrom']").$("name").val();
    if (name == null || name == "") {
        isPassed = false;
        nameAlert.html("* field 'name' must be filled");
    }

    var surname = document.forms["updateForm"]["surname"].value;
    if (surname == null || surname == "") {
        isPassed = false;
        surnameAlert.html("* field 'surname' must be filled")
    }

    var email = document.forms["updateForm"]["email"].value;
    var reg = /^([A-Za-z0-9_\-\.])+@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (!reg.test(email)) {
        isPassed = false;
        emailAlert.html("* invalid email");
    }

    return isPassed;
}

function validateRegistration() {
    var isPassed = validateUpdate();
}

function validatePassword() {
    
}

function validateGroupCreation() {
    var nameAlert = $("#groupNameAlert");
    nameAlert.html("");
    var name = $("#groupName");
    if (name.attr("value") == null || name.attr("value") == "") {
        nameAlert.html("group name couldn't be empty");
        return false;
    }

    return true;
}