$(document).ready(function()
{
    //load first page
    var params = {
        pageNumber : 1,
        accountId : $("#accountId").attr("value")
    };
    $("#wall").load("/wall", params);

    //page loading with ajax
    $('ul.pagination li').click(function(e)
    {
        var text = $(this).attr("title");
        var params = {
            pageNumber : text,
            accountId : $("#accountId").attr("value")
        };
        $("#wall").load("/wall", params);
    });
});
