function validatePhoneName(phoneName) {
    if (phoneName == null || phoneName == "") {
        alert("phone name must be not empty!");
        return false;
    }
    return true;
}

function validatePhone(phone) {
    var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    if (phone.match(phoneno)) {
        return true;
    }
    else {
        alert("invalid phone number!");
        return false;
    }
}

function addPhoneToTable(phone, elm) {
    var phoneNode = document.createTextNode(phone);
    elm.appendChild(phoneNode);
}

function addInput(elm) {
    //if (elm.getElementsByTagName('input').length > 0) return;
    var value = elm.innerHTML;
    elm.innerHTML = '';

    var input = document.createElement('input');
    input.setAttribute('type', 'text');
    input.setAttribute('value', value);
    //input.setAttribute('onBlur', 'closeInput(this)');
    elm.appendChild(input);
    //input.focus();
}

function deleteRow(elm) {
    //alert("Row index is: " + elm.parentElement.rowIndex);
    document.getElementById("phones").deleteRow(elm.parentElement.rowIndex);
}

function addDelButton(elm) {
    var button = document.createElement('button');
    button.setAttribute("class", "btn btn-danger")
    button.innerHTML = '<span class="glyphicon glyphicon-remove"></span>' //"-";
    button.onclick = function () {
        deleteRow(elm)
    };

    elm.appendChild(button)
}

function addRow() {
    var newPhone = $("#newPhone").val();
    if (validatePhone(newPhone)) {
        var table = document.getElementById("phones");
        var row = table.insertRow(0);
        var phoneCell = row.insertCell(-1);
        var phoneNameCell = row.insertCell(0);
        phoneNameCell.innerHTML = document.getElementById('newPhoneName').value;
        addPhoneToTable(newPhone, phoneCell);//addInput(inputCell);
        addDelButton(row.insertCell(-1));
    }
}

function tableToArray() {
    var array = [];
    var table = document.getElementById("phones");

    $("table#phones tr").each(function () {
        var thisRowArray = [];
        var tableData = $(this).find('td');
        if (tableData.length > 0) {
            tableData.each(function () {
                thisRowArray.push($(this).text());
            });
            array.push(thisRowArray);
        }
    });

    //document.getElementById("phonesArray").value = table;
    $("#phonesArray").val(array);
    //return array;
}