package com.getjavajob.drugova.common.model;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "account")
//@FilterDef(name="friendFilter", parameters={
//        @ParamDef( name="accepted", type="int" )})
public class Account extends BaseEntity {
    private String name;
    private String surname;
    private String patronymic;
    //@Transient
    @Column(name = "birth")
    @Convert(converter = LongDateConverter.class)
    private Date birthDate;
    @Transient
    private String personalPhone;
    @Transient
    private String workPhone;
    private String email;
    private String icq;
    private String skype;
    private String address;
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "id_account", referencedColumnName = "id", nullable = false)
    private List<Phone> phones = new ArrayList<>();
    @Lob
    private byte[] photo;
    private String password;
    //    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//    @JoinTable(name = "friend_request", joinColumns = {
//            @JoinColumn(name = "id_from", nullable = false, updatable = false) },
//            inverseJoinColumns = { @JoinColumn(name = "id_to",
//                    nullable = false, updatable = false) })
//    @FilterJoinTable(name="friendFilter", condition=":accepted = true")
    @Transient
    private List<Account> friends;
    @OneToMany(mappedBy = "accountFrom", fetch = FetchType.LAZY)
    private List<FriendRequest> outFriendRequests;
    @OneToMany(mappedBy = "accountTo", fetch = FetchType.LAZY)
    private List<FriendRequest> inFriendRequests;
    @Transient
    private List<Group> groups;
    @OneToMany(mappedBy = "accountReceiver")
    @OrderBy("date DESC")
    private List<Message> inMessages;
    @OneToMany(mappedBy = "author")
    @OrderBy("date DESC")
    private List<Message> outMessages;
    @OneToMany(mappedBy = "account", fetch = FetchType.LAZY)
    private Set<GroupRequest> groupRequests;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getPersonalPhone() {
        return personalPhone;
    }

    public void setPersonalPhone(String personalPhone) {
        this.personalPhone = personalPhone;
    }

    public String getWorkPhone() {
        return workPhone;
    }

    public void setWorkPhone(String workPhone) {
        this.workPhone = workPhone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIcq() {
        return icq;
    }

    public void setIcq(String icq) {
        this.icq = icq;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones.clear();
        if (phones != null) {
            this.phones.addAll(0, phones);
        }
        //this.phones = phones;
    }

    public void addPhones(List<Phone> phones) {
        this.phones.addAll(phones);
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public List<Account> getFriends() {
        return friends;
    }

    public void setFriends(List<Account> friends) {
        this.friends = friends;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void addFriend(Account account) {
        if (friends == null) {
            friends = new ArrayList<>();
        }
        friends.add(account);
    }

    public void deleteFriend(Account account) {
        friends.remove(friends.indexOf(account));
    }

    public void addGroup(Group group) {
        if (groups == null) {
            groups = new ArrayList<>();
        }
        groups.add(group);
    }

    public void deleteGroup(Group group) {
        groups.remove(groups.indexOf(group));
    }

    public List<FriendRequest> getOutFriendRequests() {
        return outFriendRequests;
    }

    public void setOutFriendRequests(List<FriendRequest> outFriendRequests) {
        this.outFriendRequests = outFriendRequests;
    }

    public List<FriendRequest> getInFriendRequests() {
        return inFriendRequests;
    }

    public void setInFriendRequests(List<FriendRequest> inFriendRequests) {
        this.inFriendRequests = inFriendRequests;
    }

    public List<Message> getInMessages() {
        return inMessages;
    }

    public void setInMessages(List<Message> inMessages) {
        this.inMessages = inMessages;
    }

    public List<Message> getOutMessages() {
        return outMessages;
    }

    public void setOutMessages(List<Message> outMessages) {
        this.outMessages = outMessages;
    }

    public Set<GroupRequest> getGroupRequests() {
        return groupRequests;
    }

    public void setGroupRequests(Set<GroupRequest> groupRequests) {
        this.groupRequests = groupRequests;
    }

    public Account() {
    }

    public Account(String name, String surname, String email) {
        this.name = name;
        this.surname = surname;
        this.email = email;
    }

    public Account(int id, String name, String surname, int birthDate, String personalPhone, String email) {
        this(name, surname, email);
        this.id = id;
        this.birthDate = new Date((long) birthDate * 1000);
        this.personalPhone = personalPhone;
    }

    public Account(String name, String surname, int birthDate, String personalPhone, String email) {
        this(name, surname, email);
        this.birthDate = new Date((long) birthDate * 1000);
        this.personalPhone = personalPhone;
    }

    public Account(int id, String name, String surname, Date birthDate, String personalPhone, String email) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.personalPhone = personalPhone;
        this.email = email;
    }

    public Account(String name, String surname, Date birthDate, String personalPhone, String email) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.personalPhone = personalPhone;
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Account account = (Account) o;

        if (name != null ? !name.equals(account.name) : account.name != null) return false;
        if (surname != null ? !surname.equals(account.surname) : account.surname != null) return false;
        if (patronymic != null ? !patronymic.equals(account.patronymic) : account.patronymic != null) return false;
        if (birthDate != null ? !birthDate.equals(account.birthDate) : account.birthDate != null) return false;
        if (email != null ? !email.equals(account.email) : account.email != null) return false;
        if (skype != null ? !skype.equals(account.skype) : account.skype != null) return false;
        return address != null ? address.equals(account.address) : account.address == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (patronymic != null ? patronymic.hashCode() : 0);
        result = 31 * result + (birthDate != null ? birthDate.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (skype != null ? skype.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        return result;
    }
}
