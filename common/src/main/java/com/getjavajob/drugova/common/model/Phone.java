package com.getjavajob.drugova.common.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Александр on 11.06.2016.
 */
@Entity
@Table(name = "phone")
public class Phone extends BaseEntity {
    private String name;
    private String number;
    @Column(name = "is_main")
    private boolean isMain;

//    @ManyToOne
//    @JoinColumn(name = "id_account")
//    private Account account;

    public Phone() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public boolean isMain() {
        return isMain;
    }

    public void setMain(boolean main) {
        isMain = main;
    }

//    public Account getAccount() {
//        return account;
//    }

//    public void setAccount(Account account) {
//        this.account = account;
//    }

    public Phone(String name, String number) {
        this.name = name;
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Phone phone = (Phone) o;

        if (isMain != phone.isMain) return false;
        if (!name.equals(phone.name)) return false;
        return number.equals(phone.number);

    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + number.hashCode();
        result = 31 * result + (isMain ? 1 : 0);
        return result;
    }
}
