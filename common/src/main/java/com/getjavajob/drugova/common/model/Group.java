package com.getjavajob.drugova.common.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by Александр on 03.06.2016.
 */
@Entity
@Table(name = "user_group")
public class Group extends BaseEntity {
    private String name;
    @Column(name = "creation_date")
    @Convert(converter = LongDateConverter.class)
    private Date creationDate;
    @OneToOne
    @JoinColumn(name = "id_creator_account")
    private Account creator;
    private String description;
    @Transient
    private List<Account> members = new ArrayList<>();
    @Transient
    private List<Account> admins = new ArrayList<>();
    private byte[] image;
    @OneToMany(mappedBy = "group", fetch = FetchType.LAZY)
    private Set<GroupRequest> groupRequests;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Account getCreator() {
        return creator;
    }

    public void setCreator(Account creator) {
        this.creator = creator;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Account> getMembers() {
        return members;
    }

    public void setMembers(List<Account> members) {
        this.members = members;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] photo) {
        this.image = photo;
    }

    public void addMember(Account member) {
        members.add(member);
    }

    public void addMembers(List<Account> members) {
        this.members.addAll(members);
    }

    public Set<GroupRequest> getGroupRequests() {
        return groupRequests;
    }

    public void setGroupRequests(Set<GroupRequest> groupRequests) {
        this.groupRequests = groupRequests;
    }

    public Group() {}

    public Group(int id, String name, Date creationDate) {
        this.id = id;
        this.name = name;
        this.creationDate = creationDate;
    }

    public Group(int id, String name, int creationDate) {
        this.id = id;
        this.name = name;
        this.creationDate = new Date((long) creationDate * 1000);
    }

    public Group(int id, String name, Date creationDate, Account creator) {
        this.id = id;
        this.name = name;
        this.creationDate = creationDate;
        this.creator = creator;
    }

    public Group(int id, String name, int creationDate, Account creator) {
        this.id = id;
        this.name = name;
        this.creationDate = new Date((long) creationDate * 1000);
        this.creator = creator;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Group)) return false;

        Group group = (Group) o;

        if (!name.equals(group.name)) return false;
        if (creationDate != null ? !creationDate.equals(group.creationDate) : group.creationDate != null) return false;
        if (creator != null ? !creator.equals(group.creator) : group.creator != null) return false;
        return !(description != null ? !description.equals(group.description) : group.description != null);

    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        result = 31 * result + (creator != null ? creator.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }
}
