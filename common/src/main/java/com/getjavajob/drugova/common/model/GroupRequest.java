package com.getjavajob.drugova.common.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "account_in_group")
public class GroupRequest extends BaseEntity {
    @ManyToOne//(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_account")
    private Account account;
    @ManyToOne//(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_group")
    private Group group;
    private boolean accepted;
    @Column(name = "request_date")
    @Convert(converter = LongDateConverter.class)
    private Date requestDate;
    @Column(name = "account_type")
    @Enumerated(EnumType.STRING)
    private AccountInGroupType accountType;

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public AccountInGroupType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountInGroupType accountType) {
        this.accountType = accountType;
    }

    public GroupRequest() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GroupRequest that = (GroupRequest) o;

        if (accepted != that.accepted) return false;
        if (account != null ? !account.equals(that.account) : that.account != null) return false;
        return group != null ? group.equals(that.group) : that.group == null;

    }

    @Override
    public int hashCode() {
        int result = account != null ? account.hashCode() : 0;
        result = 31 * result + (group != null ? group.hashCode() : 0);
        result = 31 * result + (accepted ? 1 : 0);
        return result;
    }

    public enum AccountInGroupType {
        USER,
        MODERATOR,
        ADMIN
    }
}
