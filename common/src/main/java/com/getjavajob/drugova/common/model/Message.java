package com.getjavajob.drugova.common.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "message")
public class Message extends BaseEntity {
    @ManyToOne//(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_author")
    private Account author;
    @ManyToOne//(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_target_account")
    private Account accountReceiver;
    @Transient
    private Group groupReceiver;
    @Convert(converter = LongDateConverter.class)
    private Date date;
    private String text;
    @Enumerated(EnumType.STRING)
    private MessageType type;
    private byte[] image;

    public Account getAuthor() {
        return author;
    }

    public void setAuthor(Account sender) {
        this.author = sender;
    }

    public Account getAccountReceiver() {
        return accountReceiver;
    }

    public void setAccountReceiver(Account accountReceiver) {
        this.accountReceiver = accountReceiver;
    }

    public Group getGroupReceiver() {
        return groupReceiver;
    }

    public void setGroupReceiver(Group groupReceiver) {
        this.groupReceiver = groupReceiver;
    }

    public MessageType getTarget() {
        return type;
    }

    public void setTarget(MessageType target) {
        this.type = target;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Message() {
    }

    public enum MessageType {
        PRIVATE,
        WALL
    }
}
