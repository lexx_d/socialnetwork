package com.getjavajob.drugova.common.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "friend_request")
public class FriendRequest extends BaseEntity {
    @ManyToOne//(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_from")
    private Account accountFrom;

    @ManyToOne//(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_to")
    private Account accountTo;

    private boolean accepted;

    public Account getAccountTo() {
        return accountTo;
    }

    public void setAccountTo(Account accountTo) {
        this.accountTo = accountTo;
    }

    public Account getAccountFrom() {
        return accountFrom;
    }

    public void setAccountFrom(Account accountFrom) {
        this.accountFrom = accountFrom;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }
}
