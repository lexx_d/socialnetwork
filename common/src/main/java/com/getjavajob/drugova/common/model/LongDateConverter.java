package com.getjavajob.drugova.common.model;

import javax.persistence.AttributeConverter;
import java.util.Date;

public class LongDateConverter implements AttributeConverter<Date, Long> {
    @Override
    public Long convertToDatabaseColumn(Date attribute) {
        return attribute.getTime() / 1000;
    }

    @Override
    public Date convertToEntityAttribute(Long dbData) {
        return new Date(dbData * 1000);
    }
}
