package com.getjavajob.drugova.sevice;

import com.getjavajob.drugova.common.model.Account;
import com.getjavajob.drugova.dao.AccountDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {
    @InjectMocks
    private AccountService accountService;

    @Mock
    private AccountDao accountDao;

    @Test
    public void getAccountById() throws Exception {
        Account expectedAccount = new Account(1, "Vasya", "Petrov", 1465064172, "+79991234588", "somemail@gmail.com");
        expectedAccount.setSkype("vasya_skype");
        when(accountDao.getObjectById(1)).thenReturn(expectedAccount);
        Account actualAccount = accountService.getAccountById(1);
        assertEquals(expectedAccount, actualAccount);
        verify(accountDao, times(1)).getObjectById(1);
    }

    @Test
    public void updateAccount() throws Exception {
        Account expectedAccount = new Account(1, "Vasya1", "Petrov1", 1465064172, "+79991234581", "somemail1@gmail.com");
        expectedAccount.setSkype("vasya_skype1");
        expectedAccount.setAddress("address1");
        accountService.updateAccount(expectedAccount);
        verify(accountDao, times(1)).updateObject(expectedAccount);
        //Account updatedAccount = accountDao.getObjectById(1);
        //assertEquals(expectedAccount, updatedAccount);
    }

}