package com.getjavajob.drugova.sevice;

import com.getjavajob.drugova.common.model.Account;
import com.getjavajob.drugova.common.model.FriendRequest;
import com.getjavajob.drugova.common.model.Message;
import com.getjavajob.drugova.dao.AccountDao;
import com.getjavajob.drugova.dao.daoexceptions.DaoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class AccountService {
    private AccountDao accountDao;

    public AccountDao getAccountDao() {
        return accountDao;
    }

    public void setAccountDao(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @Autowired
    public AccountService(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    public int createAccount(Account newAccount) throws DaoException {
        //todo: hash password here
        return accountDao.insertObject(newAccount);
    }

    public List<Account> getAllAccounts() throws DaoException {
        List<Account> allAccounts = accountDao.getAll();
        return allAccounts;
    }

    public Account getAccountById(int accId) throws DaoException {
        Account account = accountDao.getObjectById(accId);
        return account;
    }

    public Account getAccountByMailAndPassword(String email, String password) throws DaoException {
        Account account = accountDao.getAccountByMailAndPassword(email, password);
        return account;
    }

    public void updateAccount(Account account) throws DaoException {
        accountDao.updateObject(account);
    }

    public void addFriendRequest(int idFrom, int idTo) throws DaoException {
        Account accountFrom = accountDao.getObjectById(idFrom);
        Account accountTo = accountDao.getObjectById(idTo);
        accountDao.addFriendRequest(accountFrom, accountTo);
    }

    public void acceptFriendRequest(FriendRequest friendRequest) throws DaoException {
        accountDao.acceptFriendRequest(friendRequest);
    }

    public void deleteFriendRequest(int friendRequestId) throws DaoException {
        accountDao.deleteFriendRequest(friendRequestId);
    }

    public void addAccountMessage(int idAccFrom, int idAccTo, Message.MessageType type, String text, byte[] image) throws DaoException {
        accountDao.addAccountMessage(idAccFrom, idAccTo, type, text, image);
    }

    public long getWallMessagesCount(int accId) throws DaoException {
        return accountDao.getWallMessagesCount(accId);
    }

    public List<Message> getWallMessages(int accId, int beginPos, int endPos) throws DaoException {
        return accountDao.getWallMessages(accId, beginPos, endPos);
    }

    public void deleteMessage(int id) throws DaoException {
        accountDao.deleteMessage(id);
    }

    public List<Message> getMessagesBetweenAccounts(int firstAccId, int secondAccId) throws DaoException {
        return accountDao.getMessagesBetweenAccounts(firstAccId, secondAccId);
    }
}
