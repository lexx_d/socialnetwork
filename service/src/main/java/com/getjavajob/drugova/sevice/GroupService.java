package com.getjavajob.drugova.sevice;

import com.getjavajob.drugova.common.model.Account;
import com.getjavajob.drugova.common.model.Group;
import com.getjavajob.drugova.common.model.GroupRequest;
import com.getjavajob.drugova.dao.AccountDao;
import com.getjavajob.drugova.dao.GroupDao;
import com.getjavajob.drugova.dao.daoexceptions.DaoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class GroupService {
    private GroupDao groupDao;
    @Autowired
    private AccountDao accountDao;

    public GroupDao getGrouptDao() {
        return groupDao;
    }

    public void setGroupDao(GroupDao groupDao) {
        this.groupDao = groupDao;
    }

    public AccountDao getAccountDao() {
        return accountDao;
    }

    public void setAccountDao(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @Autowired
    public GroupService(GroupDao groupDao) {
        this.groupDao = groupDao;
    }

    public List<Group> getAllGroups() throws DaoException {
        List<Group> allGroups = groupDao.getAll();
        return allGroups;
    }

    public Group getGroupById(int accId) throws DaoException {
        return groupDao.getObjectById(accId);
    }

    public List<Group> getAccountGroups(int accountId) throws DaoException {
        return groupDao.getAccountGroups(accountId);
    }

    public int createGroup(int creatorId, String name, String description, byte[] image) throws DaoException {
        Group newGroup = new Group();
        Account groupCreator = accountDao.getObjectById(creatorId);
        newGroup.setCreator(groupCreator);
        newGroup.setDescription(description);
        newGroup.setCreationDate(new Date(System.currentTimeMillis()));
        newGroup.setImage(image);
        return groupDao.insertObject(newGroup);
    }

    public List<GroupRequest> getAcceptedGroupRequestsByGroupId(int groupId, int amount) throws DaoException {
        return groupDao.getAcceptedGroupRequestsByGroupId(groupId, amount);
    }

    public List<GroupRequest> getAcceptedGroupRequestsByGroupId(int groupId) throws DaoException {
        return groupDao.getAcceptedGroupRequestsByGroupId(groupId);
    }

    public List<GroupRequest> getNotAcceptedGroupRequestsByGroupId(int groupId) throws DaoException {
        return groupDao.getNotAcceptedGroupRequestsByGroupId(groupId);
    }

    public List<GroupRequest> getAdminsRequestsByGroupId(int groupId, int amount) throws DaoException {
        return groupDao.getAdminsRequestsByGroupId(groupId, amount);
    }

    public List<GroupRequest> getAdminsRequestsByGroupId(int groupId) throws DaoException {
        return groupDao.getAdminsRequestsByGroupId(groupId);
    }

    public GroupRequest getGroupRequstById(int id) throws DaoException {
        return groupDao.getGroupRequestById(id);
    }

    public GroupRequest updateGroupRequest(GroupRequest groupRequest) throws DaoException {
        return groupDao.updateGroupRequest(groupRequest);
    }

    public GroupRequest updateGroupRequest(Account account, GroupRequest groupRequest) throws DaoException {
        return groupDao.updateGroupRequest(groupRequest);
    }

    public void removeGroupRequestById(int groupRequestId) throws DaoException {
        groupDao.removeGroupRequestById(groupRequestId);
    }

    public void removeGroupRequestById(Account account, int groupRequestId) throws DaoException {
        groupDao.removeGroupRequestById(groupRequestId);
    }
}
