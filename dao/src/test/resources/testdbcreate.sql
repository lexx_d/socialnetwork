-- MySQL Workbench Forward Engineering

-- SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
-- SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
-- SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema social-network
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `social-network`;

-- -----------------------------------------------------
-- Schema social-network
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `social-network`;
USE `social-network`;

-- SET SCHEMA "SOCIAL-NETWORK";

-- -----------------------------------------------------
-- Table `social-network`.`account`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `social-network`.`account`;

CREATE TABLE IF NOT EXISTS `social-network`.`account` (
  `id`             INT(11)      NOT NULL AUTO_INCREMENT,
  `name`           VARCHAR(52)  NOT NULL,
  `surname`        VARCHAR(52)  NOT NULL,
  `patronymic`     VARCHAR(52)  NULL     DEFAULT NULL,
  `birth`          BIGINT(20)   NULL,
  `perconal_phone` VARCHAR(20)  NOT NULL,
  `work_phone`     VARCHAR(20)  NULL     DEFAULT NULL,
  `email`          VARCHAR(80)  NOT NULL,
  `icq`            VARCHAR(45)  NULL     DEFAULT NULL,
  `skype`          VARCHAR(80)  NULL     DEFAULT NULL,
  `password`       VARCHAR(512) NOT NULL,
  `address`        VARCHAR(100) NULL     DEFAULT NULL,
  `photo`          MEDIUMBLOB   NULL     DEFAULT NULL,
  PRIMARY KEY (`id`),
  -- UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC),
  UNIQUE INDEX `perconal_phone_UNIQUE` (`perconal_phone` ASC)
);
-- ENGINE = InnoDB
-- AUTO_INCREMENT = 6
-- DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table `social-network`.`group`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `social-network`.`user_group`;

CREATE TABLE IF NOT EXISTS `social-network`.`user_group` (
  `id`                 INT(11)      NOT NULL AUTO_INCREMENT,
  `id_creator_account` INT(11)      NULL     DEFAULT NULL,
  `name`               VARCHAR(45)  NOT NULL,
  `image`              BLOB         NULL     DEFAULT NULL,
  `description`        VARCHAR(550) NULL     DEFAULT NULL,
  `creation_date`      BIGINT(20)   NULL     DEFAULT NULL,
  -- UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC),
  -- INDEX `creator_account_id_idx` (`id_creator_account` ASC),
  CONSTRAINT `creator_account_id`
  FOREIGN KEY (`id_creator_account`)
  REFERENCES `social-network`.`account` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE
);
-- ENGINE = InnoDB
-- AUTO_INCREMENT = 3
-- DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `social-network`.`account_in_group`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `social-network`.`account_in_group`;

CREATE TABLE IF NOT EXISTS `social-network`.`account_in_group` (
  `id`           INT(11)     NOT NULL AUTO_INCREMENT,
  `id_account`   INT(11)     NOT NULL,
  `id_group`     INT(11)     NOT NULL,
  `accepted`     TINYINT(1)  NULL DEFAULT NULL,
  `request_date` BIGINT(20)  NULL DEFAULT NULL,
  `account_type` VARCHAR(15) NULL DEFAULT NULL
  COMMENT 'USER, MODERATOR, ADMIN',
  PRIMARY KEY (`id`),
  -- INDEX `group_id_idx` (`id_group` ASC),
  CONSTRAINT `account_id`
  FOREIGN KEY (`id_account`)
  REFERENCES `social-network`.`account` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `group_id`
  FOREIGN KEY (`id_group`)
  REFERENCES `social-network`.`user_group` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
-- ENGINE = InnoDB
-- DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `social-network`.`friend_request`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `social-network`.`friend_request`;

CREATE TABLE IF NOT EXISTS `social-network`.`friend_request` (
  `id`       INT(11)    NOT NULL  AUTO_INCREMENT,
  `id_from`  INT(11)    NOT NULL,
  `id_to`    INT(11)    NOT NULL,
  `accepted` TINYINT(1) NULL      DEFAULT NULL,
  PRIMARY KEY (`id`),
  -- INDEX `to_id_idx` (`id_to` ASC),
  CONSTRAINT `from_id`
  FOREIGN KEY (`id_from`)
  REFERENCES `social-network`.`account` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `to_id`
  FOREIGN KEY (`id_to`)
  REFERENCES `social-network`.`account` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
-- ENGINE = InnoDB
-- DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `social-network`.`text`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `social-network`.`message`;

CREATE TABLE IF NOT EXISTS `social-network`.`message` (
  `id`                INT(11)      NOT NULL AUTO_INCREMENT,
  `id_author`         INT(11)      NOT NULL,
  `text`              VARCHAR(512) NULL     DEFAULT NULL,
  `image`             BLOB         NULL     DEFAULT NULL,
  `date`              INT(11)      NULL     DEFAULT NULL,
  `id_target_account` INT(11)      NULL     DEFAULT NULL,
  `id_target_group`   INT(11)      NULL     DEFAULT NULL,
  `type`              VARCHAR(10)  NULL     DEFAULT '0'
  COMMENT 'text type: private, wall',
  PRIMARY KEY (`id`, `id_author`),
  -- INDEX `author_id_idx` (`id_author` ASC),
  -- INDEX `target_account_id_idx` (`id_target_account` ASC),
  -- INDEX `target_group_id_idx` (`id_target_group` ASC),
  CONSTRAINT `author_id`
  FOREIGN KEY (`id_author`)
  REFERENCES `social-network`.`account` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `target_account_id`
  FOREIGN KEY (`id_target_account`)
  REFERENCES `social-network`.`account` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `target_group_id`
  FOREIGN KEY (`id_target_group`)
  REFERENCES `social-network`.`user_group` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);
-- ENGINE = InnoDB
--  DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `social-network`.`phone`;

CREATE TABLE `social-network`.`phone` (
  `id`         INT         NOT NULL AUTO_INCREMENT,
  `id_account` INT         NOT NULL,
  `name`       VARCHAR(45) NULL,
  `number`     VARCHAR(45) NULL,
  `is_main`    TINYINT(1)  NULL,
  PRIMARY KEY (`id`),
  -- INDEX `id_account_idx` (`id_account` ASC
-- ),
  CONSTRAINT `id_account`
  FOREIGN KEY (`id_account`)
  REFERENCES `social-network`.`account` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

-- SET SQL_MODE=@OLD_SQL_MODE;
-- SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
-- SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
