INSERT INTO `social-network`.`account`
(`name`,`surname`,`patronymic`,`birth`,`perconal_phone`,`work_phone`,`email`,`icq`,`skype`,`password`)
VALUES
('Vasya','Petrov',NULL,1465064172,'+79991234588',NULL,'somemail@gmail.com',NULL,'vasya_skype','qqq'),
('Lena','Petrova',NULL,1465064172,'+79991224548',NULL,'lena@gmail.com',NULL,'lena','qqq'),
('Pavel','Valov',NULL,1465064172,'+79991214588',NULL,'pavel@gmail.com',NULL,'pavel_skype','qqq'),
('Irina','Frolova',NULL,1465064172,'+79991274578',NULL,'frolova@gmail.com',NULL,'irina_skype','qqq'),
('Katya','Anina',NULL,1465064172,'+79991238588',NULL,'kty@gmail.com',NULL,'katya_skype','qqq');

INSERT INTO `social-network`.`user_group`
(`id_creator_account`,`name`,`description`,`creation_date`)
VALUES
(1,'vasya-group','vasya created this group',1465064172),
(2,'katya-group','katya created this group',1465064172);

INSERT INTO `social-network`.`account_in_group`
(`id_account`,`id_group`,`accepted`,`request_date`, `account_type`)
VALUES
(2,1,true,1465064172, 'MODERATOR'),
(3,1,true,1465064172, 'ADMIN'),
(3,2,true,1465064172, 'USER'),
(4,2,true,1465064172, 'USER'),
(5,2,true,1465064172, 'USER'),
(5,1,false,1465064172,'USER');

INSERT INTO `social-network`.`friend_request`
(`id_from`,`id_to`,`accepted`)
VALUES
(1,2,false),
(1,3,true),
(1,4,true),
(2,5,true),
(3,4,true);

INSERT INTO `social-network`.`phone`
(`id_account`,`name`,`number`,`is_main`)
VALUES
(1,'Home','4990648055',false),
(1,'Work','4958885534',false),
(1,'Personal','9160648055',true);

INSERT INTO `social-network`.`message`
(`id_author`, `text`, `image`, `date`,
`id_target_account`, `id_target_group`, `type`)
VALUES
(1, 'Hello, Lena', NULL, 1465064172,
2, NULL, 'PRIVATE'),
(3, 'Test MSG 2 (to wall) ', NULL, 1377333047, -- 13 year
1, NULL, 'WALL'),
(1, 'Hello Lena (to wall) :)', NULL, 1465064173, -- 16 year
2, NULL, 'WALL'),
(2, 'Hello, Vasya! :)', NULL, 1465064181,
1, NULL, 'PRIVATE'),
(2, 'Hello, Vasya! (to wall) :)', NULL, 1465064182,
1, NULL, 'WALL'),
(3, 'Test MSG 2 (to wall) ', NULL, 1408869047, -- 14 year
1, NULL, 'WALL'),
(2, 'Test MSG 1 (to wall) ', NULL, 1472027447, -- 16 year
1, NULL, 'WALL'),
(3, 'Test MSG 2 (to wall) ', NULL, 1440405047, -- 15 year
1, NULL, 'WALL');

INSERT INTO `social-network`.`message`
(`id_author`, `text`, `date`, `id_target_account`, `type`)
VALUES
(1, 'Hello', 1469701130, 2, 'PRIVATE'),
(2, 'Hello', 1469701190, 1, 'PRIVATE'),
(1, ' how can I help you today ?', 1469701370, 2, 'PRIVATE'),
(2, 'I want to buy a new shoes.', 1469701430, 1, 'PRIVATE'),
(1, 'Shipment is free. You ll get your shoes tomorrow!', 1469701490, 2, 'PRIVATE'),
(2, 'Wow, that s great!', 1469705090, 1, 'PRIVATE'),
(2, 'Ok. Thanks for the answer. Appreciated', 1469794910, 1, 'PRIVATE'),
(1, 'You are welcome! Is there anything else I can do for you today?', 1469798510, 2, 'PRIVATE'),
(2, 'Nope, That s it.', 1469799230, 1, 'PRIVATE'),
(1, 'Thank you for contacting us today', 1469800850, 2, 'PRIVATE');


