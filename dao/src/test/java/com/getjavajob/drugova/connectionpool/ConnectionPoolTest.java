package com.getjavajob.drugova.connectionpool;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;

import static org.junit.Assert.*;


/**
 * Created by Александр on 11.06.2016.
 */
public class ConnectionPoolTest {
    private CustomConnectionPool connectionPool;

    @Before
    public void onBefore() {
        connectionPool = new CustomConnectionPool();
    }

    @After
    public void onAfter() throws Exception {
        connectionPool.close();
    }

    @Test
    public void testGetConnection() throws Exception {
        BlockingQueue<Connection> connectionBlockingQueue = new LinkedBlockingQueue<>();
        testThreadConnectionConsumer connConsumer1 = new testThreadConnectionConsumer(connectionPool, connectionBlockingQueue);
        testThreadConnectionConsumer connConsumer2 = new testThreadConnectionConsumer(connectionPool, connectionBlockingQueue);
        connConsumer1.start();
        Connection conn1 = connectionBlockingQueue.take();
        Connection conn2 = connectionBlockingQueue.take();
        connConsumer2.start();
        Connection conn3 = connectionBlockingQueue.take();
        Connection conn4 = connectionBlockingQueue.take();
        assertSame(conn1, conn2);
        assertNotSame(conn1, conn3);
        assertSame(conn3, conn4);
        connConsumer1.releaseConnection();
        connConsumer2.releaseConnection();
    }

    @Test
    public void testReleaseConnection() throws Exception {
        CountDownLatch latch = new CountDownLatch(3);
        connectionPool.setMaxConnectionsCount(2);
        BlockingQueue<Connection> connectionBlockingQueue = new LinkedBlockingQueue<>();
        testThreadConnectionConsumer connConsumer1 = new testThreadConnectionConsumer(connectionPool, connectionBlockingQueue, latch);
        testThreadConnectionConsumer connConsumer2 = new testThreadConnectionConsumer(connectionPool, connectionBlockingQueue, latch);
        testThreadConnectionConsumer connConsumer3 = new testThreadConnectionConsumer(connectionPool, connectionBlockingQueue, latch);

        connConsumer1.start();
        while (connectionBlockingQueue.size() < 2) ;

        connConsumer2.start();
        while (connectionBlockingQueue.size() < 4) ;

        connConsumer3.start();
        Thread.sleep(300);
        assertTrue(connConsumer3.isAlive());
        assertEquals(4, connectionBlockingQueue.size());

        connConsumer1.releaseConnection();

        latch.await();
        assertEquals(6, connectionBlockingQueue.size());

        //todo проверить третий конекшн
        connConsumer2.releaseConnection();
        connConsumer3.releaseConnection();
        connConsumer1.join();
        connConsumer2.join();
        connConsumer3.join();
    }
}

class testThreadConnectionConsumer extends Thread {
    private CountDownLatch putConnectionLatch;
    private ConnectionPool pool;
    private Connection connection;
    volatile private BlockingQueue<Connection> connectionQueue;
    volatile private boolean isFinished = false;

    public testThreadConnectionConsumer(ConnectionPool pool, BlockingQueue<Connection> connectionQueue, CountDownLatch putConnectionLatch) {
        this.pool = pool;
        this.connectionQueue = connectionQueue;
        this.putConnectionLatch = putConnectionLatch;
    }

    public testThreadConnectionConsumer(ConnectionPool pool, BlockingQueue<Connection> connectionQueue) {
        this.pool = pool;
        this.connectionQueue = connectionQueue;
    }

    public void releaseConnection() throws Exception {
        isFinished = true;
    }

    @Override
    public void run() {
        try {
            for (int i = 0; i < 2; i++) {
                connection = pool.getConnection();
                connectionQueue.put(connection);
            }

            if (putConnectionLatch != null) {
                putConnectionLatch.countDown();
            }
            while (!isFinished) ; // just holds connection

            connection.close();
            connection.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
    }
}