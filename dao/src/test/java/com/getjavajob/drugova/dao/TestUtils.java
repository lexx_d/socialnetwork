package com.getjavajob.drugova.dao;

import org.h2.tools.RunScript;

import javax.sql.DataSource;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by Александр on 03.07.2016.
 */
public class TestUtils {
    public void initTestDb(DataSource dataSource) throws SQLException {
        //create db
        InputStream in = this.getClass().getClassLoader().getResourceAsStream("testdbcreate.sql");
        Reader reader = new InputStreamReader(in);
        Connection connection = dataSource.getConnection();
        RunScript.execute(connection, reader);
        //insert data in db
        in = this.getClass().getClassLoader().getResourceAsStream("testdbinsert.sql");
        reader = new InputStreamReader(in);
        RunScript.execute(connection, reader);
        connection.close();
    }
}
