package com.getjavajob.drugova.dao;

import com.getjavajob.drugova.common.model.Account;
import com.getjavajob.drugova.common.model.FriendRequest;
import com.getjavajob.drugova.common.model.Message;
import com.getjavajob.drugova.common.model.Phone;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

import static org.junit.Assert.*;

/**
 * Created by Александр on 04.06.2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:socialNetworkDao-context.xml",
        "classpath:socialNetworkDao-context-overrides.xml"})
@Transactional
public class AccountDaoTest {
    //private JNDIConnectionPool connectionPool;
    @Autowired
    private DataSource dataSource;
    @Autowired
    private AccountDao accountDao;
    @Autowired
    private TestUtils testUtils;
    @PersistenceContext
    private EntityManager entityManager;

    @Before
    public void onBefore() throws SQLException, IOException, NamingException {
        accountDao.clearCache();
        testUtils.initTestDb(dataSource);
    }

    @After
    public void onAfter() throws SQLException {
        //connectionPool.close();
    }

    @Test
    public void testGetAll() throws Exception {
        List<Account> accounts = accountDao.getAll();
        assertEquals(5, accounts.size());
        //assertSame(accounts.get(0), accounts.get(1).getFriends().get(0));
    }

    @Ignore
    @Test
    public void testGetAcceptedFriends() throws Exception {
        //AccountDao accountDao = new AccountDao(connectionPool);
        List<Account> accounts = accountDao.getAcceptedFriends(2);
        Collections.sort(accounts, new Comparator<Account>() {
            @Override
            public int compare(Account o1, Account o2) {
                return o1.getId() - o2.getId();
            }
        });
        List<Account> expectedAccounts = new ArrayList<>();
        expectedAccounts.add(accountDao.getObjectById(1));
        expectedAccounts.add(accountDao.getObjectById(5));

        assertEquals(expectedAccounts, accounts);
    }

    @Test
    //@Transactional
    public void testDeleteById() throws Exception {
        //entityManager.clear();
        accountDao.deleteObjectById(4);
        entityManager.flush();
        Account acc = accountDao.getObjectById(4);
        //entityManager.refresh(acc);
        assertNull(acc);
    }

    @Test
    public void testGetAccountById() throws Exception {
        Account expectedAccount = new Account(1, "Vasya", "Petrov", 1465064172, "+79991234588", "somemail@gmail.com");
        expectedAccount.setSkype("vasya_skype");
        Account actualAccount = accountDao.getObjectById(1);
        assertEquals(expectedAccount, actualAccount);
    }

    @Test
    public void testInsertAccount() throws Exception {
        Account newAccount = new Account("VasyaTest", "PetrovTest", 1465126134, "+71111234588", "vasyatest@gmail.com");
        newAccount.setSkype("test_skype");

        int newAccId = accountDao.insertObject(newAccount);
        Account insertedAccount = accountDao.getObjectById(newAccId);
        assertEquals(newAccount, insertedAccount);
    }

    @Test
    public void testUpdateAccount() throws Exception {
        List<Account> accounts = accountDao.getAll();
        Account expectedAccount = new Account(1, "Vasya1", "Petrov1", 1465064172, "+79991234581", "somemail1@gmail.com");
        expectedAccount.setSkype("vasya_skype1");
        expectedAccount.setAddress("address1");
        expectedAccount = accountDao.updateObject(expectedAccount);
        entityManager.flush();
        entityManager.clear();
        Account updatedAccount = accountDao.getObjectById(1);
        assertEquals(expectedAccount, updatedAccount);
    }

    @Test
    public void testHibernateGetAll() throws Exception {
        List<Account> accounts = accountDao.getAll();
        assertEquals(5, accounts.size());
        //assertSame(accounts.get(0), accounts.get(1).getFriends().get(0));
    }

    @Test
    public void testGetAccountByMailAndPassword() throws Exception {
        Account expectedAccount = new Account(1, "Vasya", "Petrov", 1465064172, "+79991234588", "somemail@gmail.com");
        expectedAccount.setSkype("vasya_skype");
        Account actualAccount = accountDao.getAccountByMailAndPassword("somemail@gmail.com", "qqq");
        assertEquals(expectedAccount, actualAccount);
    }

    @Test
    public void testAddFriendRequest() throws Exception {
        Account accountFrom = accountDao.getObjectById(5);
        Account accountTo = accountDao.getObjectById(1);
        accountDao.addFriendRequest(accountFrom, accountTo);
        entityManager.flush();
        Account newAccountFrom = accountDao.getObjectById(5);
        assertEquals(1, newAccountFrom.getOutFriendRequests().size());
        assertSame(newAccountFrom.getOutFriendRequests().get(0).getAccountFrom(), newAccountFrom);
        assertSame(newAccountFrom.getOutFriendRequests().get(0).getAccountTo(), accountTo);
    }

    @Test
    public void testAcceptFriendRequest() throws Exception {
        Account accountId2 = accountDao.getObjectById(2);
        FriendRequest friendRequest = accountId2.getInFriendRequests().get(0);
        accountDao.acceptFriendRequest(friendRequest);
        entityManager.flush();
        Account updatedAccountId2 = accountDao.getObjectById(2);
        assertEquals(true, updatedAccountId2.getInFriendRequests().get(0).isAccepted());
    }

    @Test
    public void testDeleteFriendRequest() throws Exception {
        Account accountId2 = accountDao.getObjectById(2);
        FriendRequest friendRequest = accountId2.getInFriendRequests().get(0);
        accountDao.deleteFriendRequest(friendRequest.getId());
        entityManager.flush();
        entityManager.clear();
        Account accountId1 = accountDao.getObjectById(1);
        Account updatedAccountId2 = accountDao.getObjectById(2);
        assertEquals(2, accountId1.getOutFriendRequests().size());
        assertEquals(0, updatedAccountId2.getInFriendRequests().size());
    }

    @Test
    public void testAddPhone() throws Exception {
        Account accountId1 = accountDao.getObjectById(1);
        Phone phone = new Phone("home_1", "89239991111");
        List<Phone> newPhones = new ArrayList<>();
        newPhones.add(phone);
        accountId1.getPhones().add(phone);
        accountDao.updateObject(accountId1);
        entityManager.flush();
        entityManager.clear();
        Account updatedAccountId1 = accountDao.getObjectById(1);
        assertEquals(4, updatedAccountId1.getPhones().size());
    }

    @Test
    public void testGetMessages() throws Exception {
        Account accountId1 = accountDao.getObjectById(1);
        assertEquals(11, accountId1.getInMessages().size());
        assertEquals(7, accountId1.getOutMessages().size());
    }

    @Test
    public void testAddMessage() throws Exception {
        Account accountId1 = accountDao.getObjectById(1);
        Message messageId1ToId2 = new Message();
        messageId1ToId2.setAuthor(accountId1);

        Account accountId2 = accountDao.getObjectById(2);
        messageId1ToId2.setAccountReceiver(accountId2);

        String text = "new message";
        final int idAccountFrom = 1;
        final int idAccountTo = 2;
        accountDao.addAccountMessage(idAccountFrom, idAccountTo, Message.MessageType.PRIVATE, text);
        entityManager.refresh(accountId1);
        entityManager.refresh(accountId2);

        assertEquals(8, accountId1.getOutMessages().size());
        assertEquals(11, accountId1.getInMessages().size());

        assertEquals(8, accountId2.getOutMessages().size());
        assertEquals(8, accountId2.getInMessages().size());
    }

    @Test
    public void getWallMessages() throws Exception {
        //test message count
        List<Message> wallMessages = accountDao.getWallMessages(1, 0, 10);
        assertEquals(5, wallMessages.size());
        //test message order
        int[] messageYears = new int[5];
        int i = 0;
        for (Message message : wallMessages) {
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(message.getDate());
            int year = calendar.get(Calendar.YEAR);
            messageYears[i++] = year;
        }
        int[] expectedMessageYears = new int[]{2016, 2016, 2015, 2014, 2013};
        assertArrayEquals(expectedMessageYears, messageYears);

        List<Message> wallMessagesFrom2To4 = accountDao.getWallMessages(1, 2, 4);
        assertEquals(3, wallMessagesFrom2To4.size());
    }

    @Test
    public void getWallMessagesCount() throws Exception {
        assertEquals(5, accountDao.getWallMessagesCount(1));
    }

    @Test
    public void testGetMessagesBeetwenAccounts() throws Exception {
        List<Message> messages = accountDao.getMessagesBetweenAccounts(1, 2);
        assertEquals(12, messages.size());

        List<Message> messages2 = accountDao.getMessagesBetweenAccounts(2, 1);
        assertEquals(12, messages2.size());
    }
}