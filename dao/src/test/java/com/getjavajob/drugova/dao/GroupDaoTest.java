package com.getjavajob.drugova.dao;

import com.getjavajob.drugova.common.model.Account;
import com.getjavajob.drugova.common.model.Group;
import com.getjavajob.drugova.common.model.GroupRequest;
import com.getjavajob.drugova.dao.daoexceptions.DaoException;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertSame;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:socialNetworkDao-context.xml",
        "classpath:socialNetworkDao-context-overrides.xml"})
@Transactional
public class GroupDaoTest {
    @Autowired
    private DataSource dataSource;
    @Autowired
    private GroupDao groupDao;
    @Autowired
    private AccountDao accountDao;
    @Autowired
    TestUtils testUtils;
    @PersistenceContext
    private EntityManager entityManager;

    @Before
    public void onBefore() throws SQLException, IOException {
        //connectionPool = new CustomConnectionPool();
        groupDao.clearCache();
        testUtils.initTestDb(dataSource);
    }

    @After
    public void onAfter() throws SQLException {
        //connectionPool.close();
    }

    @Test
    public void testGetAll() throws Exception {
        List<Group> groups = groupDao.getAll();
        assertEquals(2, groups.size());
    }

    @Ignore
    @Test
    public void testSameGroupOfMember() throws DaoException {
        List<Group> groups = groupDao.getAll();
        assertEquals(2, groups.size());
        Group groupId1 = null;
        for (Group group : groups) {
            if (group.getId() == 1) {
                groupId1 = group;
                break;
            }
        }
        Account accountId1 = null;
        for (GroupRequest groupRequest : groupId1.getGroupRequests()) {
            if (groupRequest.getAccount().getId() == 1) {
                accountId1 = groupRequest.getAccount();
                break;
            }
        }

        Group accountId1GroupId1 = null;
        for (GroupRequest groupRequest : accountId1.getGroupRequests()) {
            if (groupRequest.getGroup().getId() == 1) {
                accountId1GroupId1 = groupRequest.getGroup();
                break;
            }
        }

        assertSame(groupId1, accountId1GroupId1);
    }

    @Test
    public void testGetGroupById() throws DaoException {
        Group group = groupDao.getObjectById(1);
        assertEquals("vasya-group", group.getName());
        assertEquals("vasya created this group", group.getDescription());
        assertEquals(3, group.getGroupRequests().size());
    }

    @Test
    public void testCreateGroup() throws Exception {
        Group newGroup = new Group();
        Account groupCreator = accountDao.getObjectById(1);
        newGroup.setCreator(groupCreator);
        newGroup.setDescription("test description");
        newGroup.setCreationDate(new Date(System.currentTimeMillis()));
        int idInsertedGroup = groupDao.insertObject(newGroup);
        entityManager.flush();
        entityManager.clear();
        Group insertedGroup = groupDao.getObjectById(idInsertedGroup);
        assertEquals("test description", insertedGroup.getDescription());
        assertEquals(newGroup.getCreationDate().getTime()/1000, insertedGroup.getCreationDate().getTime()/1000);
        assertEquals(newGroup.getCreator().getId(), insertedGroup.getCreator().getId());
    }

    @Test
    public void testGetAcceptedGroupRequestsByAccountId() throws DaoException {
        List<GroupRequest> groupRequests = groupDao.getAcceptedGroupRequestsByAccountId(3);
        assertEquals(2, groupRequests.size());
    }

    @Test
    public void testGetAcceptedGroupRequestsByGroupId() throws DaoException {
        List<GroupRequest> groupRequests = groupDao.getAcceptedGroupRequestsByGroupId(1);
        assertEquals(2, groupRequests.size());

        List<GroupRequest> someGroupReaqests = groupDao.getAcceptedGroupRequestsByGroupId(1, 1);
        assertEquals(1, someGroupReaqests.size());
    }

    @Test
    public void testGetGroupAdmins() throws Exception {
        List<GroupRequest> adminsRequests = groupDao.getAdminsRequestsByGroupId(1, 1);
        assertEquals(1, adminsRequests.size());
        assertNotEquals("USER", adminsRequests.get(0).getAccountType().toString());

        List<GroupRequest> allAdminRequests = groupDao.getAdminsRequestsByGroupId(1);
        assertEquals(2, allAdminRequests.size());
    }
}