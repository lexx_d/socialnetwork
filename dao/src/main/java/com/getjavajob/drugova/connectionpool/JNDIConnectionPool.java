package com.getjavajob.drugova.connectionpool;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by Александр on 24.06.2016.
 */
public class JNDIConnectionPool implements ConnectionPool {
    private DataSource ds;
    private ThreadLocal<Integer> givenConnectionsCnt;
    private ThreadLocal<Connection> threadLocalConnection;
    private ThreadLocal<Connection> threadLocalSourceConnection;

    public JNDIConnectionPool() throws NamingException, SQLException {
        Context initContext = new InitialContext();
        Context envContext = (Context) initContext.lookup("java:/comp/env");
        ds = (DataSource) envContext.lookup("jdbc/social-network");

        givenConnectionsCnt = new ThreadLocal<>();
        givenConnectionsCnt.set(0);
        threadLocalConnection = new ThreadLocal<>();
        threadLocalConnection.set(null);
        threadLocalSourceConnection = new ThreadLocal<>();
    }

    public JNDIConnectionPool(DataSource ds) {
        this.ds = ds;

        givenConnectionsCnt = new ThreadLocal<>();
        givenConnectionsCnt.set(0);
        threadLocalConnection = new ThreadLocal<>();
        threadLocalConnection.set(null);
        threadLocalSourceConnection = new ThreadLocal<>();
    }

    public Connection getConnection() throws SQLException {
        initLocalThreadVariables();

        if (threadLocalConnection.get() != null) {
            givenConnectionsCnt.set(givenConnectionsCnt.get() + 1);
            return threadLocalConnection.get();
        }

        Connection connection = ds.getConnection();
        givenConnectionsCnt.set(givenConnectionsCnt.get() + 1);
        //wrap connection
        SocNetConInvokationHandler handler = new SocNetConInvokationHandler(connection, this);
        Connection proxyConnection = (Connection) Proxy.newProxyInstance(Connection.class.getClassLoader(),
                new Class[]{Connection.class}, handler);
        threadLocalConnection.set(proxyConnection);
        threadLocalSourceConnection.set(connection);
        return proxyConnection;
    }

    @Override
    public void releaseConnection(Connection connection) throws SQLException {
        givenConnectionsCnt.set(givenConnectionsCnt.get() - 1);
        if (givenConnectionsCnt.get().equals(0)) {
            threadLocalConnection.set(null);
            threadLocalSourceConnection.get().close();
        }
    }

    private void initLocalThreadVariables() {
        if (givenConnectionsCnt.get() == null) {
            givenConnectionsCnt.set(0);
        }
    }
}
