package com.getjavajob.drugova.connectionpool;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by Александр on 05.06.2016.
 */
public interface ConnectionPool {

    Connection getConnection() throws SQLException;

    void releaseConnection(Connection connection) throws SQLException;

}
