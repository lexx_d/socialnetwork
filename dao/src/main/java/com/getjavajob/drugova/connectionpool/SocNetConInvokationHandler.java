package com.getjavajob.drugova.connectionpool;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.sql.Connection;

/**
 * Created by Александр on 08.06.2016.
 */
public class SocNetConInvokationHandler implements InvocationHandler {
    private Connection connection;
    private ConnectionPool pool;

    public SocNetConInvokationHandler(Connection connection, ConnectionPool pool) {
        this.connection = connection;
        this.pool = pool;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (method.getName().equals("close")) {
            pool.releaseConnection(connection);
            return null;
        }
        return method.invoke(connection, args);
    }
}
