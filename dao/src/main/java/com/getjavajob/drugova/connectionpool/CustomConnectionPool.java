package com.getjavajob.drugova.connectionpool;

import java.io.IOException;
import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Created by Александр on 05.06.2016.
 */
public class CustomConnectionPool implements ConnectionPool {
    private int maxConnectionsCount = 5;
    private BlockingQueue<Connection> availibleConnections = new ArrayBlockingQueue<>(10);
    private BlockingQueue<Connection> busyConnections = new ArrayBlockingQueue<>(10);
    private ThreadLocal<Integer> givenConnectionsCnt;
    private ThreadLocal<Connection> threadLocalConnection;
    private String driver;
    private String url;
    private String user;
    private String password;

    public int getMaxConnectionsCount() {
        return maxConnectionsCount;
    }

    public void setMaxConnectionsCount(int maxConnectionsCount) {
        this.maxConnectionsCount = maxConnectionsCount;
    }

    public CustomConnectionPool() {
        Properties props = new Properties();
        try {
            props.load(this.getClass().getClassLoader()
                    .getResourceAsStream("social-network-db.properties"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        driver = props.getProperty("database.driver");
        url = props.getProperty("database.url");
        user = props.getProperty("database.user");
        password = props.getProperty("database.password");

        givenConnectionsCnt = new ThreadLocal<>();
        givenConnectionsCnt.set(0);
        threadLocalConnection = new ThreadLocal<>();
        threadLocalConnection.set(null);
    }

    public Connection getConnection() throws SQLException {
        initLocalThreadVariables();

        if (threadLocalConnection.get() != null) {
            givenConnectionsCnt.set(givenConnectionsCnt.get() + 1);
            return threadLocalConnection.get();
        }

        try {
            if (availibleConnections.isEmpty()) {
                //create connection if possible
                createConnection();
            }
            Connection connection = availibleConnections.take();
            busyConnections.put(connection);
            givenConnectionsCnt.set(givenConnectionsCnt.get() + 1);
            //wrap connection
            SocNetConInvokationHandler handler = new SocNetConInvokationHandler(connection, this);
            Connection proxyConnection = (Connection) Proxy.newProxyInstance(Connection.class.getClassLoader(),
                    new Class[]{Connection.class}, handler);
            threadLocalConnection.set(proxyConnection);
            return proxyConnection;
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public void releaseConnection(Connection connection) {
        givenConnectionsCnt.set(givenConnectionsCnt.get() - 1);
        if (givenConnectionsCnt.get().equals(0)) {
            threadLocalConnection.set(null);
            busyConnections.remove(connection);
            try {
                availibleConnections.put(connection);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }

    private void createConnection() throws SQLException {
        if (busyConnections.size() + availibleConnections.size() >= maxConnectionsCount) {
            return;
        }

        if (driver != null) {
            try {
                Class.forName(driver).newInstance();
            } catch (ClassNotFoundException | IllegalAccessException | InstantiationException ex) {
                ex.printStackTrace();
            }
        }

        Connection connection = DriverManager.getConnection(url, user, password);
        try {
            availibleConnections.put(connection);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    public void close() throws SQLException {
        for (Connection connection : availibleConnections) {
            connection.close();
        }
    }

    private void initLocalThreadVariables() {
        if (givenConnectionsCnt.get() == null) {
            givenConnectionsCnt.set(0);
        }
    }

}
