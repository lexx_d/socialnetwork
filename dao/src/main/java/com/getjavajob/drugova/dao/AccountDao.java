package com.getjavajob.drugova.dao;

import com.getjavajob.drugova.common.model.*;
import com.getjavajob.drugova.dao.daoexceptions.DaoException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.sql.DataSource;
import java.sql.*;
import java.util.*;
import java.util.Date;

/**
 * Created by Александр on 11.06.2016.
 */
public class AccountDao extends AbstractDao<Account> {
    private static Map<Integer, Account> loadedAccounts = new HashMap<>();
    private static Set<Integer> finishedAccounts = new HashSet<>();

    private GroupDao groupDao;

    public AccountDao(DataSource dataSource, JdbcTemplate jdbcTemplate, GroupDao groupDao) {
        super(dataSource, jdbcTemplate);
        this.groupDao = groupDao;
    }

    public AccountDao(DataSource dataSource, JdbcTemplate jdbcTemplate) {
        super(dataSource, jdbcTemplate);
    }

    public AccountDao(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    protected Account createObjectFromResult(ResultSet resultSet) throws DaoException {
        try {
            //finding account in cache
            int id = resultSet.getInt("id");
            if (loadedAccounts.containsKey(id)) {
                return loadedAccounts.get(id);
            }

            Account account = new Account(resultSet.getInt("id"), resultSet.getString("name"), resultSet.getString("surname"),
                    resultSet.getInt("birth"), resultSet.getString("perconal_phone"), resultSet.getString("email"));
            account.setSkype(resultSet.getString("skype"));
            account.setPatronymic(resultSet.getString("patronymic"));
            account.setWorkPhone(resultSet.getString("work_phone"));
            appendComplexObjectAttributes(account);
            return account;
        } catch (SQLException ex) {
            throw new DaoException(ex);
        }
    }

    @Override
    protected void appendComplexObjectAttributes(Account account) throws DaoException {
        if (!loadedAccounts.containsKey(account.getId())) {
            loadedAccounts.put(account.getId(), account);
        }
        //addFriends(account, new HashSet<Integer>());
        addFriends(account);
        addGroups(account);
        addPhones(account);
    }

    private void addFriends(Account account/*, Set<Integer> finishedAccounts*/) throws DaoException {
        if (finishedAccounts.contains(account.getId())) {
            return;
        }
        finishedAccounts.add(account.getId());
        List<Account> friends = getAcceptedFriends(account.getId());
        for (Account friend : friends) {
            int friendId = friend.getId();
            if (loadedAccounts.containsKey(friendId)) {
                account.addFriend(loadedAccounts.get(friendId));
            } else {
                //account.addFriend(friend);
                loadedAccounts.put(friendId, friend);
                account.addFriend(loadedAccounts.get(friendId));
            }
        }
        //finishedAccounts.add(account.getId());

        for (Account friend : friends) {
            addFriends(friend/*, finishedAccounts*/);
        }
    }

    private void addGroups(Account account) throws DaoException {
        List<Group> groups = new GroupDao(dataSource, jdbcTemplate).getAccountGroups(account.getId());
        for (Group group : groups) {
            account.addGroup(group);
        }
    }

    private void addPhones(Account account) throws DaoException {
        List<Phone> phones = new PhoneDao(dataSource, jdbcTemplate).getAccountPhones(account.getId());
        account.setPhones(phones);
    }

    @Override
    protected String selectAllString() {
        return "SELECT * FROM account";
    }

    @Override
    protected String selectByIdString() {
        return "SELECT * FROM account WHERE id = ?";
    }

    @Override
    protected String deleteByIdString() {
        return "DELETE FROM account WHERE id = ?";
    }

    @Override
    protected String insertObjectString() {
        return "INSERT INTO account (`name`, `surname`," +
                "`patronymic`,`birth`,`perconal_phone`,`work_phone`," +
                "`email`, `icq`, `skype`, `password`, `address`) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    }

    @Override
    protected String updateObjectString() {
        return "UPDATE account SET" +
                "`name` = ?," +
                "`surname` = ?," +
                "`patronymic` = ?," +
                "`birth` = ?," +
                "`perconal_phone` = ?," +
                "`work_phone` = ?," +
                "`email` = ?," +
                "`icq` = ?," +
                "`skype` = ?," +
                "`password` = ?," +
                "`address` = ?," +
                "`photo` = ?" +
                "WHERE `id` = ?;";
    }

    @Override
    protected PreparedStatementSetter getUpdatePreparedStatementSetter(final Account obj) throws SQLException {
        return new PreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps) throws SQLException {
                setInsertPreparedStatementAttributes(ps, obj);
                ps.setInt(13, obj.getId());
            }
        };
    }

    @Override
    protected void setInsertPreparedStatementAttributes(PreparedStatement insertStatement, Account newAccount) throws SQLException {
        insertStatement.setString(1, newAccount.getName());
        insertStatement.setString(2, newAccount.getSurname());
        if (newAccount.getPatronymic() == null) {
            insertStatement.setNull(3, Types.VARCHAR);
        } else {
            insertStatement.setString(3, newAccount.getPatronymic());
        }

        if (newAccount.getBirthDate() == null) {
            insertStatement.setNull(4, Types.VARCHAR);
        } else {
            insertStatement.setLong(4, newAccount.getBirthDate().getTime() / 1000);
        }

        if (newAccount.getPersonalPhone() == null) {
            insertStatement.setNull(5, Types.VARCHAR);
        } else {
            insertStatement.setString(5, newAccount.getPersonalPhone());
        }

        if (newAccount.getWorkPhone() == null) {
            insertStatement.setNull(6, Types.VARCHAR);
        } else {
            insertStatement.setString(6, newAccount.getWorkPhone());
        }

        insertStatement.setString(7, newAccount.getEmail());

        if (newAccount.getIcq() == null) {
            insertStatement.setNull(8, Types.VARCHAR);
        } else {
            insertStatement.setString(8, newAccount.getIcq());
        }

        if (newAccount.getSkype() == null) {
            insertStatement.setNull(9, Types.VARCHAR);
        } else {
            insertStatement.setString(9, newAccount.getSkype());
        }

        if (newAccount.getPassword() == null) {
            insertStatement.setNull(10, Types.VARCHAR);
        } else {
            insertStatement.setString(10, newAccount.getPassword());
        }

        if (newAccount.getAddress() == null) {
            insertStatement.setNull(11, Types.VARCHAR);
        } else {
            insertStatement.setString(11, newAccount.getAddress());
        }

        if (newAccount.getPhoto() == null) {
            insertStatement.setNull(12, Types.VARCHAR);
        } else {
            insertStatement.setBytes(12, newAccount.getPhoto());
        }
    }

    public List<Account> getAcceptedFriends(int accId) throws DaoException {
        List<Account> friends = new ArrayList<>();
        final String SELECT_FRIENDS = "SELECT * FROM friend_request WHERE (id_from = ? OR id_to = ?)" +
                "AND accepted = true";

        try (Connection connection = dataSource.getConnection();
             PreparedStatement selectFriendsStatement = connection.prepareStatement(SELECT_FRIENDS)) {
            selectFriendsStatement.setInt(1, accId);
            selectFriendsStatement.setInt(2, accId);
            try (ResultSet resultSet = selectFriendsStatement.executeQuery()) {
                while (resultSet.next()) {
                    int friendId = resultSet.getInt("id_from") == accId ? resultSet.getInt("id_to") : resultSet.getInt("id_from");
                    friends.add(getObjectById(friendId));
                }
            }
        } catch (SQLException ex) {
            throw new DaoException(ex);
        }

        return friends;
    }

    public List<Account> getGroupMembers(int groupId) throws DaoException {
        List<Account> members = new ArrayList<>();
        final String selectMembersById = "SELECT id_account FROM account_in_group WHERE id_group = ? AND accepted = true";

        try (Connection connection = dataSource.getConnection();
             PreparedStatement selectMembersStatement = connection.prepareStatement(selectMembersById)) {

            selectMembersStatement.setInt(1, groupId);
            try (ResultSet resultSet = selectMembersStatement.executeQuery()) {
                while (resultSet.next()) {
                    members.add(getObjectById(resultSet.getInt("id_account")));
                }
            }

        } catch (SQLException ex) {
            throw new DaoException(ex);
        }

        return members;
    }

    public Account getAccountByMailAndPassword(String email, String password) throws DaoException {
        try {
            EntityManager em = entityManager;//entityManagerFactory.createEntityManager();
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Account> criteriaQuery = cb.createQuery(Account.class);
            Root<Account> from = criteriaQuery.from(Account.class);
            //EntityType<Account> Account_ = from.getModel();
            CriteriaQuery<Account> select = criteriaQuery.select(from).
                    where(cb.equal(from.get(Account_.email), email),
                            cb.equal(from.get(Account_.password), password));
            return em.createQuery(select).getSingleResult();
        } catch (Exception ex) {
            throw new DaoException(ex);
        }
    }

    public void clearCache() {
        loadedAccounts.clear();
        finishedAccounts.clear();
    }

    public List<Account> getAllAccounts() {
        EntityManager em = entityManager;//entityManagerFactory.createEntityManager();
        try {
            //em.getTransaction().begin();
            CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
            CriteriaQuery<Account> criteriaQuery = criteriaBuilder.createQuery(Account.class);
            CriteriaQuery<Account> select = criteriaQuery.select(criteriaQuery.from(Account.class));
            List<Account> accounts = em.createQuery(select).getResultList();
            //em.getTransaction().commit();
            return accounts;
        } finally {
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
        }
    }

    public void addFriendRequest(Account accFrom, Account accTo) throws DaoException {
        try {
            Account accountFrom = entityManager.find(Account.class, accFrom.getId());
            Account accountTo = entityManager.find(Account.class, accTo.getId());
            FriendRequest friendRequest = new FriendRequest();
            friendRequest.setAccountFrom(accountFrom);
            friendRequest.setAccountTo(accountTo);
            friendRequest.setAccepted(false);
            entityManager.persist(friendRequest);
        } catch (PersistenceException e) {
            throw new DaoException(e);
        }
    }

    public void acceptFriendRequest(FriendRequest friendRequest) throws DaoException {
        try {
            friendRequest.setAccepted(true);
            entityManager.merge(friendRequest);
        } catch (PersistenceException e) {
            throw new DaoException(e);
        }
    }

    public void deleteFriendRequest(int id) throws DaoException {
        try {
            FriendRequest friendRequestForDelete = entityManager.find(FriendRequest.class, id);
            entityManager.remove(friendRequestForDelete);
        } catch (PersistenceException e) {
            throw new DaoException(e);
        }
    }

    public void addAccountMessage(int idAccFrom, int idAccTo, Message.MessageType type, String text, byte[] image) throws DaoException {
        try {
            Account accountFrom = entityManager.find(Account.class, idAccFrom);
            Account accountTo = entityManager.find(Account.class, idAccTo);
            Message message = new Message();
            message.setAuthor(accountFrom);
            message.setAccountReceiver(accountTo);
            message.setText(text);
            message.setImage(image);
            message.setTarget(type);
            message.setDate(new Date(System.currentTimeMillis()));
            entityManager.persist(message);
        } catch (PersistenceException e) {
            throw new DaoException(e);
        }
    }

    public void addAccountMessage(int idAccFrom, int idAccTo, Message.MessageType type, String text) throws DaoException {
        addAccountMessage(idAccFrom, idAccTo, type, text, null);
    }

    public void addGroupMessage(Account accFrom, Group groupTo) throws DaoException {

    }

    public List<Message> getWallMessages(int accId, int beginPos, int endPos) throws DaoException {
        try {
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<Message> criteriaQuery = cb.createQuery(Message.class);
            Root<Message> from = criteriaQuery.from(Message.class);
            CriteriaQuery<Message> select =
                    criteriaQuery.select(from)
                            .where(cb.equal(from.get(Message_.accountReceiver), accId),
                                    cb.equal(from.get(Message_.type), Message.MessageType.WALL));
            select.orderBy(cb.desc(from.get(Message_.date)));
            return entityManager.createQuery(select)
                    .setFirstResult(beginPos).setMaxResults(endPos).getResultList();
        } catch (PersistenceException e) {
            throw new DaoException(e);
        }
    }

    public long getWallMessagesCount(int accId) throws DaoException {
        try {
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<Long> cq = cb.createQuery(Long.class);
            Root<Message> from = cq.from(Message.class);
            cq.select(cb.count(from));
            cq.where(cb.equal(from.get(Message_.accountReceiver), accId),
                    cb.equal(from.get(Message_.type), Message.MessageType.WALL));
            return entityManager.createQuery(cq).getSingleResult();
        } catch (PersistenceException e) {
            throw new DaoException(e);
        }
    }

    public void deleteMessage(int id) throws DaoException {
        try {
            Message messageForDelete = entityManager.find(Message.class, id);
            entityManager.remove(messageForDelete);
        } catch (PersistenceException e) {
            throw new DaoException(e);
        }
    }

    public List<Message> getMessagesBetweenAccounts(int firstAccId, int secondAccId) throws DaoException {
        try {
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<Message> cq = cb.createQuery(Message.class);
            Root<Message> from = cq.from(Message.class);
            Predicate andFirstAuthorSecondReceiver = cb.and(cb.equal(from.get(Message_.author), firstAccId),
                    cb.equal(from.get(Message_.accountReceiver), secondAccId));
            Predicate andFirstReceiverSecondAuthor = cb.and(cb.equal(from.get(Message_.accountReceiver), firstAccId),
                    cb.equal(from.get(Message_.author), secondAccId));
            Predicate or = cb.or(andFirstAuthorSecondReceiver, andFirstReceiverSecondAuthor);
            CriteriaQuery<Message> select = cq.select(from).where(or, cb.equal(from.get(Message_.type), Message.MessageType.PRIVATE));
            select.orderBy(cb.desc(from.get(Message_.date)));
            return entityManager.createQuery(select).getResultList();
        } catch (PersistenceException e) {
            logger.warn("PersistenceException was caught in getMessagesBetweenAccounts()");
            throw new DaoException(e);
        }
    }
}
