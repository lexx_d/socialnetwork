package com.getjavajob.drugova.dao;

import com.getjavajob.drugova.common.model.Phone;
import com.getjavajob.drugova.dao.daoexceptions.DaoException;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Александр on 25.06.2016.
 */
public class PhoneDao extends AbstractDao<Phone> {

    public PhoneDao(DataSource dataSource) {
        super(dataSource);
    }

    public PhoneDao(DataSource dataSource, JdbcTemplate jdbcTemplate) {
        super(dataSource, jdbcTemplate);
    }

    @Override
    protected Phone createObjectFromResult(ResultSet resultSet) throws DaoException {
        return new Phone();
        /*
        try {
            return new Phone(resultSet.getInt("id"), resultSet.getString("name"), resultSet.getString("number"));
        } catch (SQLException ex) {
            throw new DaoException(ex);
        }
        */
    }

    @Override
    protected String selectAllString() {
        return "SELECT * FROM phone";
    }

    @Override
    protected String selectByIdString() {
        return "SELECT * FROM phone WHERE id = ?";
    }

    @Override
    protected String deleteByIdString() {
        return "DELETE FROM phone WHERE id = ?";
    }

    @Override
    protected String insertObjectString() {
        return "INSERT INTO phone(id_account, name, number) VALUES(?, ?, ?);";
    }

    @Override
    protected String updateObjectString() {
        return null;
    }

    @Override
    protected PreparedStatementSetter getUpdatePreparedStatementSetter(Phone onj) throws SQLException {
        return null;
    }

    @Override
    protected void setInsertPreparedStatementAttributes(PreparedStatement preparedStatement, Phone newObject) throws SQLException {
    }

    public List<Phone> getAccountPhones(int accId) throws DaoException {
        final String selectPhones = "SELECT * FROM phone WHERE id_account = ?";
        try {
            return jdbcTemplate.query(selectPhones, new Object[]{accId}, new RowMapper<Phone>() {
                @Override
                public Phone mapRow(ResultSet rs, int rowNum) throws SQLException {
                    try {
                        return createObjectFromResult(rs);
                    } catch (DaoException e) {
                        throw new SQLException(e);
                    }
                }
            });
        } catch (DataAccessException e) {
            throw new DaoException(e);
        }
    }

}
