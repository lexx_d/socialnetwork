package com.getjavajob.drugova.dao.daoexceptions;

/**
 * Created by Александр on 10.06.2016.
 */
public class DaoException extends Exception {
    public DaoException(Throwable cause) {
        super(cause);
    }

    public DaoException(String msg) {
        super(msg);
    }

    public DaoException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
