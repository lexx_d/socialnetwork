package com.getjavajob.drugova.dao;

import com.getjavajob.drugova.common.model.BaseEntity;
import com.getjavajob.drugova.dao.daoexceptions.DaoException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.sql.DataSource;
import java.lang.reflect.ParameterizedType;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Александр on 11.06.2016.
 */
@Repository
abstract public class AbstractDao<T extends BaseEntity> {
    private Class entityBeanClass = ((Class) ((ParameterizedType) getClass()
            .getGenericSuperclass()).getActualTypeArguments()[0]);
    protected DataSource dataSource;
    @Autowired
    protected JdbcTemplate jdbcTemplate;
    //    @Autowired
//    protected EntityManagerFactory entityManagerFactory;
    @PersistenceContext
    protected EntityManager entityManager;

    protected static final Logger logger = LoggerFactory.getLogger(AbstractDao.class);

    @Autowired
    public AbstractDao(DataSource dataSource, JdbcTemplate jdbcTemplate) {
        this.dataSource = dataSource;
        this.jdbcTemplate = jdbcTemplate;
    }

    @Autowired
    public AbstractDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @SuppressWarnings("unchecked")
    public List<T> getAll() throws DaoException {
        EntityManager em = entityManager;
        try {
            CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
            CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(entityBeanClass);
            CriteriaQuery<T> select = criteriaQuery.select(criteriaQuery.from(entityBeanClass));
            return em.createQuery(select).getResultList();
        } catch (NoResultException ex) {
            logger.info("DAO method getAll() threw NoResultException");
            return null;
        } catch (PersistenceException ex) {
            logger.info("DAO method getAll() catch PersistenceException");
            throw new DaoException(ex);
        }
    }

    protected void appendComplexObjectAttributes(T object) throws DaoException {
    }

    public void deleteObjectById(int objId) throws DaoException {
        try {
            Object objForDelete = entityManager.find(entityBeanClass, objId);
            entityManager.remove(objForDelete);
        } catch (PersistenceException e) {
            logger.info("DAO method deleteObjectById() catch PersistenceException");
            throw new DaoException(e);
        }
    }

    @SuppressWarnings("unchecked")
    public T getObjectById(int objId) throws DaoException {
        EntityManager em = entityManager;
        try {
            CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
            CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(entityBeanClass);
            Root<T> from = criteriaQuery.from(entityBeanClass);
            CriteriaQuery<T> select = criteriaQuery.select(from).where(criteriaBuilder.equal(from.get("id"), objId));
            T object = em.createQuery(select).getSingleResult();
            return object;
        } catch (NoResultException ex) {
            logger.info("DAO method getObjectById() catch NoResultException");
            return null;
        } catch (PersistenceException ex) {
            logger.info("DAO method getObjectById() catch PersistenceException");
            throw new DaoException(ex);
        }
    }

    public int insertObject(T newObject) throws DaoException {
        EntityManager em = entityManager;//entityManagerFactory.createEntityManager();
        try {
            em.persist(newObject);
            //em.flush();
            //em.refresh(newObject);
            return newObject.getId();
        } catch (PersistenceException e) {
            logger.info("DAO method insertObject() catch PersistenceException");
            throw new DaoException(e);
        }
    }

    public T updateObject(T obj) throws DaoException {
        try {
            logger.info("DAO method updateObject() called");
            return entityManager.merge(obj);
        } catch (PersistenceException ex) {
            logger.info("DAO method updateObject() catch PersistenceException");
            throw new DaoException(ex);
        }
    }

    abstract protected T createObjectFromResult(ResultSet resultSet) throws DaoException;

    abstract protected String selectAllString();

    abstract protected String selectByIdString();

    abstract protected String deleteByIdString();

    abstract protected String insertObjectString();

    abstract protected String updateObjectString();

    abstract protected void setInsertPreparedStatementAttributes(PreparedStatement preparedStatement, T newObject) throws SQLException;

    abstract protected PreparedStatementSetter getUpdatePreparedStatementSetter(T onj) throws SQLException;

    class DaoRSExtractor implements ResultSetExtractor<T> {
        @Override
        public T extractData(ResultSet resultSet) throws SQLException, DataAccessException {
            try {
                return createObjectFromResult(resultSet);
            } catch (DaoException e) {
                if (e.getCause() instanceof SQLException) {
                    throw (SQLException) e.getCause();
                }
                throw new SQLException();
            }
        }
    }

    class DaoRowMapper implements RowMapper<T> {
        @Override
        public T mapRow(ResultSet resultSet, int i) throws SQLException {
            T debug = new DaoRSExtractor().extractData(resultSet);
            return debug;
        }
    }
}

