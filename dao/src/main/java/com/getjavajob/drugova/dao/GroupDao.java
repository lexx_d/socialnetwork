package com.getjavajob.drugova.dao;

import com.getjavajob.drugova.common.model.Account;
import com.getjavajob.drugova.common.model.Group;
import com.getjavajob.drugova.common.model.GroupRequest;
import com.getjavajob.drugova.common.model.GroupRequest_;
import com.getjavajob.drugova.dao.daoexceptions.DaoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;

import javax.persistence.PersistenceException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by Александр on 03.06.2016.
 */
public class GroupDao extends AbstractDao<Group> {
    private static Map<Integer, Group> loadedGroups = new HashMap<>();
    private static Set<Integer> finishedGroups = new HashSet<>();
    @Autowired
    private AccountDao accountDao;

    public GroupDao(DataSource dataSource, JdbcTemplate jdbcTemplate, AccountDao accountDao) {
        super(dataSource, jdbcTemplate);
        this.accountDao = accountDao;
    }

    public GroupDao(DataSource dataSource, JdbcTemplate jdbcTemplate) {
        super(dataSource, jdbcTemplate);
    }

    public GroupDao(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    protected Group createObjectFromResult(ResultSet resultSet) throws DaoException {
        try {
            //finding group in cache
            int id = resultSet.getInt("id");
            if (loadedGroups.containsKey(id)) {
                return loadedGroups.get(id);
            }

            Group group = new Group(resultSet.getInt("id"), resultSet.getString("name"), resultSet.getInt("creation_date"));
            loadedGroups.put(group.getId(), group);
            Account groupCreator = new AccountDao(dataSource, jdbcTemplate).getObjectById(resultSet.getInt("id_creator_account"));
            group.setDescription(resultSet.getString("description"));
            group.setCreator(groupCreator);
            appendComplexObjectAttributes(group);
            return group;
        } catch (SQLException ex) {
            throw new DaoException(ex);
        }
    }

    protected void appendComplexObjectAttributes(Group group) throws DaoException {
        addGroupMembers(group);
    }

    private void addGroupMembers(Group group) throws DaoException {
        if (finishedGroups.contains(group.getId())) {
            return;
        }

        finishedGroups.add(group.getId());
        List<Account> members = new AccountDao(dataSource, jdbcTemplate).getGroupMembers(group.getId());
        for (Account member : members) {
            group.addMember(member);
        }
    }

    public List<Group> getAccountGroups(int accountId) throws DaoException {
        List<Group> accountGroups = new ArrayList<>();
        final String selectGroupsByAccountId =
                "SELECT id_group FROM account_in_group WHERE id_account = ? AND accepted = true";
        try (Connection connection = dataSource.getConnection();
             PreparedStatement selectGroupsStatement = connection.prepareStatement(selectGroupsByAccountId)) {

            selectGroupsStatement.setInt(1, accountId);
            try (ResultSet resultSet = selectGroupsStatement.executeQuery()) {
                while (resultSet.next()) {
                    accountGroups.add(getObjectById(resultSet.getInt("id_group")));
                }
            }

        } catch (SQLException ex) {
            throw new DaoException(ex);
        }

        return accountGroups;

//        Account account = entityManager.find(Account.class, accountId);
//        try {
//            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
//            CriteriaQuery<GroupRequest> criteriaQuery = cb.createQuery(GroupRequest.class);
//            Root<GroupRequest> from = criteriaQuery.from(GroupRequest.class);
//            //EntityType<Account> Account_ = from.getModel();
//            CriteriaQuery<GroupRequest> select = criteriaQuery.select(from).
//                    where(cb.equal(from.get(GroupRequest_.account), account),
//                            cb.equal(from.get(GroupRequest_.accepted), true));
//            return entityManager.createQuery(select).getResultList();
//        } catch (Exception ex) {
//            throw new DaoException(ex);
//        }
    }

    public List<GroupRequest> getAcceptedGroupRequestsByAccountId(int accountId) throws DaoException {
        try {
            Account account = entityManager.find(Account.class, accountId);

            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<GroupRequest> criteriaQuery = cb.createQuery(GroupRequest.class);
            Root<GroupRequest> from = criteriaQuery.from(GroupRequest.class);
            CriteriaQuery<GroupRequest> select = criteriaQuery.select(from).
                    where(cb.equal(from.get(GroupRequest_.account), account),
                            cb.equal(from.get(GroupRequest_.accepted), true));
            return entityManager.createQuery(select).getResultList();
        } catch (Exception ex) {
            throw new DaoException(ex);
        }
    }

    private List<GroupRequest> getGroupRequestsByGroupId(int groupId, int amount, boolean isAccepted) throws DaoException {
        try {
            Group group = entityManager.find(Group.class, groupId);

            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<GroupRequest> criteriaQuery = cb.createQuery(GroupRequest.class);
            Root<GroupRequest> from = criteriaQuery.from(GroupRequest.class);
            CriteriaQuery<GroupRequest> select = criteriaQuery.select(from).
                    where(cb.equal(from.get(GroupRequest_.group), group),
                            cb.equal(from.get(GroupRequest_.accepted), isAccepted));

            if (amount > 0) {
                return entityManager.createQuery(select).setMaxResults(amount).getResultList();
            }
            return entityManager.createQuery(select).getResultList();
        } catch (PersistenceException e) {
            logger.warn("getAcceptedGroupRequestsByGroupId({}, {}) caught PersistenceException", groupId, amount);
            throw new DaoException(e);
        }
    }

    public List<GroupRequest> getAcceptedGroupRequestsByGroupId(int groupId, int amount) throws DaoException {
        return getGroupRequestsByGroupId(groupId, amount, true);
    }

    public List<GroupRequest> getAcceptedGroupRequestsByGroupId(int groupId) throws DaoException {
        return getAcceptedGroupRequestsByGroupId(groupId, 0);
    }

    public List<GroupRequest> getNotAcceptedGroupRequestsByGroupId(int groupId) throws DaoException {
        return getGroupRequestsByGroupId(groupId, 0, false);
    }

    public List<GroupRequest> getAdminsRequestsByGroupId(int groupId, int amount) throws DaoException {
        try {
            Group group = entityManager.find(Group.class, groupId);

            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<GroupRequest> criteriaQuery = cb.createQuery(GroupRequest.class);
            Root<GroupRequest> from = criteriaQuery.from(GroupRequest.class);
            Predicate or = cb.or(cb.equal(from.get(GroupRequest_.accountType), GroupRequest.AccountInGroupType.ADMIN),
                    cb.equal(from.get(GroupRequest_.accountType), GroupRequest.AccountInGroupType.MODERATOR));
            CriteriaQuery<GroupRequest> select = criteriaQuery.select(from).
                    where(or);

            if (amount >= 0) {
                return entityManager.createQuery(select).setMaxResults(amount).getResultList();
            }
            return entityManager.createQuery(select).getResultList();

        } catch (PersistenceException e) {
            logger.warn("getAdminsRequestsByGroupId({}, {}) caught PersistenceException", groupId, amount);
            throw new DaoException(e);
        }
    }

    public List<GroupRequest> getAdminsRequestsByGroupId(int groupId) throws DaoException {
        return getAdminsRequestsByGroupId(groupId, 0);
    }

    public GroupRequest getGroupRequestById(int id) throws DaoException {
        try {
            return entityManager.find(GroupRequest.class, id);
        } catch (PersistenceException e) {
            logger.warn("getGroupRequestById({}) caught PersistenceException", id);
            throw new DaoException(e);
        }
    }

    public GroupRequest updateGroupRequest(GroupRequest groupRequest) throws DaoException {
        try {
            return entityManager.merge(groupRequest);
        } catch (PersistenceException e) {
            throw new DaoException(e);
        }
    }

    public void removeGroupRequestById(int groupRequestId) throws DaoException {
        try {
            GroupRequest groupRequest = entityManager.find(GroupRequest.class, groupRequestId);
            entityManager.remove(groupRequest);
        } catch (PersistenceException e) {
            throw new DaoException(e);
        }
    }

    @Override
    protected String selectAllString() {
        return "SELECT * FROM `group`";
    }

    @Override
    protected String selectByIdString() {
        return "SELECT * FROM `group` WHERE id = ?";
    }

    @Override
    protected String deleteByIdString() {
        return "DELETE FROM `group` WHERE id = ?";
    }

    @Override
    protected String insertObjectString() {
        return null;
    }

    @Override
    protected String updateObjectString() {
        return null;
    }

    @Override
    protected PreparedStatementSetter getUpdatePreparedStatementSetter(Group onj) throws SQLException {
        return null;
    }

    @Override
    protected void setInsertPreparedStatementAttributes(PreparedStatement preparedStatement, Group newObject) throws SQLException {

    }

    public void clearCache() {
        loadedGroups.clear();
        finishedGroups.clear();
    }

}
